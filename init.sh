#!/bin/bash
set -e

: ${APP_ENV:?Missing environment variable}
: ${DBHOST_KINDERS:?Missing environment variable}
: ${DBNAME_KINDERS:?Missing environment variable}
: ${DBPASS_KINDERS:?Missing environment variable}

cd /app

composer dump-env $APP_ENV
bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration --env=$APP_ENV

# Create an administrator if none are defined (user=admin, pass=password)
if [[ -z "$(bin/console admin:user:list)" ]]; then
  echo "password" | bin/console admin:user:create admin > /dev/null
fi

chown www-data:www-data -R var/log var/cache

exec frankenphp run --config /etc/caddy/Caddyfile
