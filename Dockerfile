FROM registry.gitlab.com/arnapou/docker/php:8.2-dev as build

COPY --chown=www-data:www-data . /app

RUN mkdir -p -m 777 /app/var/log /app/var/cache \
 && chown www-data:www-data -R /app

USER www-data
RUN composer install --no-interaction --no-progress --optimize-autoloader \
 && mkdir -p /app/public/img

FROM registry.gitlab.com/arnapou/docker/php:8.2-frankenphp as final

COPY --from=build /app /app

RUN install-php-extensions @composer \
 && echo "APP_ENV=prod"                       >> /app/.env.local \
 && echo "APP_SECRET=$(openssl rand -hex 16)" >> /app/.env.local \
 && chown www-data:www-data -R /app

CMD ["/app/init.sh"]
