<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use App\Assert;
use App\Entity\BaseEntity;
use App\Entity\BPZ;
use App\Entity\Image;
use App\Entity\Item;
use App\Entity\Kinder;
use App\Entity\Piece;
use App\Entity\ZBA;
use App\Form\Type\Entity\BPZType;
use App\Form\Type\Entity\ImageType;
use App\Form\Type\Entity\ItemType;
use App\Form\Type\Entity\KinderType;
use App\Form\Type\Entity\PieceType;
use App\Form\Type\Entity\ZBAType;
use App\Repository\ImageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class FormFactory
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly EntityManagerInterface $entityManager,
        private readonly FormFactoryInterface $formFactory,
        private readonly Environment $templating
    ) {
    }

    /**
     * @param class-string $type
     */
    private function detectOptions(string $type, object $entity): array
    {
        return match (true) {
            ImageType::class === $type && $entity instanceof Image => ['image_type' => $entity->getType()],
            BPZType::class === $type && $entity instanceof BPZ => ['kinder' => $entity->getKinder()],
            ZBAType::class === $type && $entity instanceof ZBA => ['kinder' => $entity->getKinder()],
            KinderType::class === $type && $entity instanceof Kinder => ['serie' => $entity->getSerie()],
            ItemType::class === $type && $entity instanceof Item => ['serie' => $entity->getSerie()],
            PieceType::class === $type && $entity instanceof Piece => ['serie' => $entity->getSerie()],
            default => [],
        };
    }

    /**
     * @param class-string|null $type
     */
    private function create(BaseEntity $entity, string $type = null, array $options = []): ?FormInterface
    {
        $type = $type ?: __NAMESPACE__ . '\\Type\\Entity\\' . ImageRepository::getTypeFrom($entity) . 'Type';
        if (!class_exists($type)) {
            return null;
        }

        $options += $this->detectOptions($type, $entity);

        $form = $this->formFactory->create($type, $entity, $options);
        $request = $this->requestStack->getCurrentRequest();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newEntity = Assert::object($form->getData());

            $this->entityManager->persist($newEntity);
            $this->entityManager->flush();

            return null;
        }

        return $form;
    }

    /**
     * @param class-string|null $type
     */
    public function render(
        string $view,
        ?BaseEntity $entity,
        array $options = [],
        array $context = [],
        string $type = null
    ): ?Response {
        if (null === $entity) {
            return null;
        }

        if ($form = $this->create($entity, $type, $options)) {
            $context = array_merge($context, [
                'item' => $entity,
                'form' => $form->createView(),
            ]);

            $content = $this->templating->render($view, $context);

            return new Response($content);
        }

        return null;
    }
}
