<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use App\Assert;
use App\Entity\BaseEntity;
use App\Entity\BaseItem;
use App\Entity\Image;

use function App\Hack\Symfony5\sf5Get;

use App\Repository\ImageRepository;
use App\Service\Admin\SearchFilter;
use App\Service\ImageHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @phpstan-type ResultsAndMore array{results: array, more: bool}
 */
class AutocompleteService
{
    private const EMPTY = [
        'results' => [],
        'more' => false,
    ];

    public function __construct(
        private readonly FormFactoryInterface $formFactory,
        private readonly ManagerRegistry $doctrine,
        private readonly ImageHelper $imageHelper,
        private readonly SearchFilter $searchFilter
    ) {
    }

    /**
     * @param class-string $type
     *
     * @phpstan-return ResultsAndMore
     */
    public function images(Request $request, string $type): array
    {
        $form = $this->formFactory->create($type);
        $fieldName = Assert::string(sf5Get($request, 'field_name'));
        $fieldOptions = $form->get($fieldName)->getConfig()->getOptions();

        $dataClass = $form->get($fieldName)->getParent()?->getConfig()->getDataClass();

        if (!empty($dataClass) && class_exists($dataClass)) {
            $imageType = ImageRepository::getTypeFrom($dataClass);
        } else {
            $imageType = '';
        }

        $repo = $this->doctrine->getRepository(Assert::className($fieldOptions['class'] ?? null));
        if (!$repo instanceof ServiceEntityRepository) {
            return self::EMPTY;
        }

        $term = str_replace('*', '%', Assert::string(sf5Get($request, 'q')));

        $qbCount = $this->searchFilter->searchQueryBuilder($repo, [$term]);
        $qbCount
            ->select($qbCount->expr()->count('e'))
            ->andWhere('e.type = :type')->setParameter('type', $imageType);
        if ('' === $term) {
            $qbCount->andWhere('e.linked = :linked')->setParameter('linked', false);
        }

        $maxResults = Assert::int($fieldOptions['page_limit'] ?? 20);
        $offset = (Assert::int(sf5Get($request, 'page', 1)) - 1) * $maxResults;

        $qbResult = $this->searchFilter->searchQueryBuilder($repo, [$term]);
        $qbResult
            ->andWhere('e.type = :type')->setParameter('type', $imageType)
            ->setMaxResults($maxResults)->setFirstResult($offset);
        if ('' === $term) {
            $qbResult->andWhere('e.linked = :linked')->setParameter('linked', false);
        }

        $count = Assert::int($qbCount->getQuery()->getSingleScalarResult());
        $paginationResults = Assert::array($qbResult->getQuery()->getResult());

        return [
            'results' => $this->mapImageToArray($paginationResults),
            'more' => $count > ($offset + $maxResults),
        ];
    }

    private function mapImageToArray(array $paginationResults): array
    {
        return array_map(
            fn (Image $image) => $image->getFile()
                ? [
                    'id' => $image->getId(),
                    'text' => (string) $image,
                    'file' => $this->imageHelper->asset($image),
                ]
                : [
                    'id' => $image->getId(),
                    'text' => (string) $image,
                ],
            $paginationResults
        );
    }

    /**
     * @param class-string|null $class
     *
     * @phpstan-return ResultsAndMore
     */
    public function entities(Request $request, string $type, string $class = null): array
    {
        $form = $this->formFactory->create($type);
        $fieldName = Assert::string(sf5Get($request, 'field_name'));
        $fieldOptions = $form->get($fieldName)->getConfig()->getOptions();

        $repo = $this->doctrine->getRepository(Assert::className($class ?: $fieldOptions['class']));
        if (!$repo instanceof ServiceEntityRepository) {
            return self::EMPTY;
        }

        $term = str_replace('*', '%', Assert::string(sf5Get($request, 'q')));

        $qbCount = $this->searchFilter->searchQueryBuilder($repo, [$term]);
        $qbCount->select($qbCount->expr()->count('e'));

        $maxResults = Assert::int($fieldOptions['page_limit'] ?? 20);
        $offset = (Assert::int(sf5Get($request, 'page', 1)) - 1) * $maxResults;

        $qbResult = $this->searchFilter->searchQueryBuilder($repo, [$term]);
        $qbResult->setMaxResults($maxResults)->setFirstResult($offset);

        $count = Assert::int($qbCount->getQuery()->getSingleScalarResult());
        $paginationResults = Assert::array($qbResult->getQuery()->getResult());

        return [
            'results' => $this->mapToArray($paginationResults),
            'more' => $count > ($offset + $maxResults),
        ];
    }

    private function mapToArray(array $paginationResults): array
    {
        return array_map(
            fn (BaseEntity $entity) => ($entity instanceof BaseItem && ($image = $entity->getImage()))
                ? [
                    'id' => $entity->getId(),
                    'text' => (string) $entity,
                    'file' => $this->imageHelper->asset($image),
                ]
                : [
                    'id' => $entity->getId(),
                    'text' => (string) $entity,
                ],
            $paginationResults
        );
    }
}
