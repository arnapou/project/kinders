<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Mapping\PropertyMappingFactory;
use Vich\UploaderBundle\Storage\FileSystemStorage;
use Vich\UploaderBundle\Storage\StorageInterface;

/**
 * Decorateur de storage car la classe FileSystemStorage est finale.
 *
 * @see FileSystemStorage
 */
class VichStorage implements StorageInterface
{
    private readonly FileSystemStorage $storage;

    public function __construct(private readonly PropertyMappingFactory $factory)
    {
        $this->storage = new FileSystemStorage($factory);
    }

    public function getFactory(): PropertyMappingFactory
    {
        return $this->factory;
    }

    public function upload($obj, PropertyMapping $mapping): void
    {
        $this->storage->upload($obj, $mapping);
    }

    public function remove($obj, PropertyMapping $mapping): ?bool
    {
        return $this->storage->remove($obj, $mapping);
    }

    public function resolvePath(
        $obj,
        string $fieldName = null,
        string $className = null,
        ?bool $relative = false
    ): ?string {
        return $this->storage->resolvePath($obj, $fieldName, $className, $relative);
    }

    public function resolveUri($obj, string $fieldName = null, string $className = null): ?string
    {
        return $this->storage->resolveUri($obj, $fieldName, $className);
    }

    public function resolveStream($obj, string $fieldName, string $className = null)
    {
        return $this->storage->resolveStream($obj, $fieldName, $className);
    }
}
