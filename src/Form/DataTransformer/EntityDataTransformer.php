<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\DataTransformer;

use App\Entity\BaseEntity;
use App\Entity\BaseItem;
use App\Entity\Collection;
use Tetranz\Select2EntityBundle\Form\DataTransformer\EntityToPropertyTransformer;

class EntityDataTransformer extends EntityToPropertyTransformer
{
    /**
     * Transform entity to array.
     *
     * @param mixed|BaseItem $entity
     */
    public function transform($entity): array
    {
        if ($id = $entity instanceof BaseEntity ? $entity->getId() : null) {
            if ($entity instanceof BaseItem
                || $entity instanceof Collection
            ) {
                return [$id => $entity];
            }
        }

        return [];
    }
}
