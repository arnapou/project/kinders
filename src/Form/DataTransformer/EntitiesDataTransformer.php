<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\DataTransformer;

use App\Entity\Image;
use Tetranz\Select2EntityBundle\Form\DataTransformer\EntitiesToPropertyTransformer;

class EntitiesDataTransformer extends EntitiesToPropertyTransformer
{
    /**
     * @param mixed|Image[] $entities
     */
    public function transform($entities): array
    {
        if (empty($entities) || !is_iterable($entities)) {
            return [];
        }

        $data = [];

        foreach ($entities as $entity) {
            if ($entity instanceof Image && ($id = $entity->getId())) {
                $data[$id] = $entity;
            }
        }

        return $data;
    }
}
