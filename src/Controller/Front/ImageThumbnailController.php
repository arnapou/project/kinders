<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Front;

use App\Assert;
use App\Service\ImageHelper;
use DateTime;

use function extension_loaded;

use GdImage;
use Imagick;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\ItemInterface;
use TypeError;

class ImageThumbnailController extends AbstractController
{
    private readonly int $tnSize;
    private readonly int $tnExpire;
    private readonly FilesystemAdapter $cache;

    /** @var array<string, string> */
    private array $mimeTypes = [
        'jpg' => 'image/jpeg',
        'png' => 'image/png',
    ];

    public function __construct(
        ParameterBagInterface $parameterBag,
        private readonly ImageHelper $helper
    ) {
        $this->tnSize = Assert::int($parameterBag->get('thumbnail.size'));
        $this->tnExpire = Assert::int($parameterBag->get('thumbnail.expire'));
        $this->cache = new FilesystemAdapter('thumbnail', directory: '/cache');
    }

    #[Route(path: '/img/{path}_tn.{ext}', name: 'image_thumbnail', requirements: [
        'path' => '[a-zA-Z0-9]+/[a-z0-9]/[a-z0-9]{8}',
        'ext' => 'jpg|png',
    ])]
    #[Route(path: '/img/{path}_tn.{w}.{ext}', name: 'image_thumbnail_w', requirements: [
        'path' => '[a-zA-Z0-9]+/[a-z0-9]/[a-z0-9]{8}',
        'ext' => 'jpg|png',
        'w' => '\d+',
    ])]
    #[Route(path: '/img/{path}_tn.x{h}.{ext}', name: 'image_thumbnail_h', requirements: [
        'path' => '[a-zA-Z0-9]+/[a-z0-9]/[a-z0-9]{8}',
        'ext' => 'jpg|png',
        'h' => '\d+',
    ])]
    #[Route(path: '/img/{path}_tn.{w}x{h}.{ext}', name: 'image_thumbnail_wh', requirements: [
        'path' => '[a-zA-Z0-9]+/[a-z0-9]/[a-z0-9]{8}',
        'ext' => 'jpg|png',
        'w' => '\d+',
        'h' => '\d+',
    ])]
    public function thumbnailAction(string $path, string $ext, int $w = 0, int $h = 0): Response
    {
        $this->sanitizeWH($w, $h);
        $filename = $this->helper->getUploadDestination() . "/$path.$ext";
        if (is_file($filename)) {
            $filemtime = (int) filemtime($filename);
            $filesize = (int) filesize($filename);
            $content = Assert::string(
                $this->cache->get(
                    "{$w}x{$h}" . md5($path) . ".$ext.$filemtime.$filesize",
                    function (ItemInterface $item) use ($filename, $ext, $w, $h): string {
                        $item->expiresAfter($this->tnExpire);

                        return $this->imgResize($filename, $ext, $w, $h);
                    }
                )
            );

            return $this->fileResponse($content, $ext, $filemtime);
        }
        throw new NotFoundHttpException('Not Found');
    }

    private function fileResponse(string $content, string $ext, int $filemtime): Response
    {
        $response = new Response($content);
        $response->headers->set('Content-Type', $this->mimeTypes[$ext]);
        $response->setCache(
            [
                'etag' => base64_encode(hash('sha256', $content, true)),
                'last_modified' => DateTime::createFromFormat('U', (string) $filemtime),
                'max_age' => 864000,
                's_maxage' => 864000,
                'public' => true,
            ]
        );

        /*
         * Permet de désactiver le fait que symfony force tout à NO_CACHE quand on utilise une session.
         */
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');

        return $response;
    }

    private function imgResize(string $filename, string $ext, int $width, int $height): string
    {
        return $this->tryResizeImagick($filename, $ext, $width, $height)
            ?? $this->tryResizeGD($filename, $ext, $width, $height)
            ?? throw new RuntimeException();
    }

    private function tryResizeImagick(string $filename, string $ext, int $width, int $height): ?string
    {
        if (!extension_loaded('imagick')) {
            return null;
        }
        $img = new Imagick($filename);
        $w1 = $img->getImageWidth();
        $h1 = $img->getImageHeight();
        [$w2, $h2] = $this->newSize($w1, $h1, $width, $height);
        $img->resizeImage($w2, $h2, Imagick::FILTER_LANCZOS, 1);

        return $img->getImageBlob();
    }

    private function tryResizeGD(string $filename, string $ext, int $width, int $height): ?string
    {
        if (!extension_loaded('gd')) {
            return null;
        }

        $resize = function (false|GdImage $img) use ($width, $height): GdImage {
            if (false === $img) {
                throw new TypeError();
            }

            $w1 = imagesx($img);
            $h1 = imagesy($img);
            [$w2, $h2] = $this->newSize($w1, $h1, $width, $height);
            $dst = imagecreate($w2, $h2) ?: throw new TypeError();
            imagecopyresampled($dst, $img, 0, 0, 0, 0, $w2, $h2, $w1, $h1);

            return $dst;
        };

        $catch = static function (callable $process): string {
            ob_start();
            $process();

            return (string) ob_get_clean();
        };

        return match ($ext) {
            'jpg' => $catch(fn () => imagejpeg($resize(imagecreatefromjpeg($filename)), null, 95)),
            'png' => $catch(fn () => imagepng($resize(imagecreatefrompng($filename)), null, 9)),
            default => throw new RuntimeException()
        };
    }

    /**
     * @return array{int, int}
     */
    private function newSize(float $w1, float $h1, float $width, float $height): array
    {
        if ($w1 / $h1 > $width / $height) {
            return [(int) $width, (int) floor($width * $h1 / $w1)];
        }

        return [(int) floor($height * $w1 / $h1), (int) $height];
    }

    private function sanitizeWH(int &$w, int &$h): void
    {
        if (0 === $w && 0 === $h) {
            $w = $h = $this->tnSize;
        } elseif (0 === $w) {
            $w = 2000;
        } elseif (0 === $h) {
            $h = 2000;
        }
    }
}
