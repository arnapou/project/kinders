<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Item;
use App\Form\AutocompleteService;
use App\Form\Type\Entity\ItemType;

use function App\Hack\Symfony5\sf5Get;

use App\Repository\ItemRepository;
use App\Repository\SerieRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemsController extends AdminAbstractController
{
    #[Route(path: '/items/', name: 'admin_items')]
    public function index(ItemRepository $repository, Breadcrumb $breadcrumb, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('Items', $this->generateUrl('admin_items'));
        $searchFilter->setRouteName('admin_items');

        return $this->render('@admin/items/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/items/add', name: 'admin_items_add')]
    #[Route(path: '/items/add-{id}', name: 'admin_items_add_parent', requirements: ['id' => '\d+'])]
    public function add(Breadcrumb $breadcrumb, ?int $id, SerieRepository $serieRepository): Response
    {
        $breadcrumb->add('Items', $this->generateUrl('admin_items'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_items_add'));
        $entity = (new Item())->setSerie($serieRepository->find((int) $id));

        return $this->renderAdd('@admin/items/form.html.twig', $entity);
    }

    #[Route(path: '/items/edit-{id}', name: 'admin_items_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, ItemRepository $repository, int $id): Response
    {
        $breadcrumb->add('Items', $this->generateUrl('admin_items'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_items_edit', ['id' => $id]));

        return $this->renderEdit('@admin/items/form.html.twig', $repository->find($id));
    }

    #[Route(path: '/items/delete-{id}', name: 'admin_items_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, ItemRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_items');
    }

    #[Route(path: '/items/autocomplete', name: 'admin_items_autocomplete')]
    public function autocomplete(AutocompleteService $autocomplete, Request $request): Response
    {
        if ('images' === sf5Get($request, 'field_name')) {
            $result = $autocomplete->images($request, ItemType::class);
        } else {
            $result = $autocomplete->entities($request, ItemType::class);
        }

        return new JsonResponse($result);
    }
}
