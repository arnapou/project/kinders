<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Catalog;
use App\Repository\CatalogRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CatalogsController extends AdminAbstractController
{
    #[Route(path: '/catalogs/', name: 'admin_catalogs')]
    public function index(CatalogRepository $repository, Breadcrumb $breadcrumb, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Catalogues', $this->generateUrl('admin_catalogs'));

        return $this->render('@admin/catalogs/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/catalogs/add', name: 'admin_catalogs_add')]
    public function add(Breadcrumb $breadcrumb): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Catalogues', $this->generateUrl('admin_catalogs'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_catalogs_add'));

        return $this->renderAdd('@admin/catalogs/form.html.twig', new Catalog());
    }

    #[Route(path: '/catalogs/edit-{id}', name: 'admin_catalogs_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, CatalogRepository $repository, int $id): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Catalogues', $this->generateUrl('admin_catalogs'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_catalogs_edit', ['id' => $id]));

        return $this->renderEdit('@admin/catalogs/form.html.twig', $repository->find($id));
    }

    #[Route(path: '/catalogs/delete-{id}', name: 'admin_catalogs_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, CatalogRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_catalogs');
    }
}
