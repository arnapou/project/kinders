<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Attribute;
use App\Repository\AttributeRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AttributesController extends AdminAbstractController
{
    #[Route(path: '/attributes/', name: 'admin_attributes')]
    public function index(Breadcrumb $breadcrumb, AttributeRepository $repository, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Attributs', $this->generateUrl('admin_attributes'));
        $searchFilter->setRouteName('admin_attributes');

        return $this->render('@admin/attributes/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/attributes/add', name: 'admin_attributes_add')]
    public function add(Breadcrumb $breadcrumb): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Attributs', $this->generateUrl('admin_attributes'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_attributes_add'));

        return $this->renderAdd('@admin/attributes/form.html.twig', new Attribute());
    }

    #[Route(path: '/attributes/edit-{id}', name: 'admin_attributes_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, AttributeRepository $repository, int $id): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Attributs', $this->generateUrl('admin_attributes'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_attributes_edit', ['id' => $id]));

        return $this->renderEdit('@admin/attributes/form.html.twig', $repository->find($id));
    }

    #[Route(path: '/attributes/delete-{id}', name: 'admin_attributes_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, AttributeRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_attributes');
    }
}
