<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Piece;
use App\Form\AutocompleteService;
use App\Form\Type\Entity\PieceType;

use function App\Hack\Symfony5\sf5Get;

use App\Repository\PieceRepository;
use App\Repository\SerieRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PiecesController extends AdminAbstractController
{
    #[Route(path: '/pieces/', name: 'admin_pieces')]
    public function index(PieceRepository $repository, Breadcrumb $breadcrumb, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('Pièces', $this->generateUrl('admin_pieces'));
        $searchFilter->setRouteName('admin_pieces');

        return $this->render('@admin/pieces/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/pieces/add', name: 'admin_pieces_add')]
    #[Route(path: '/pieces/add-{id}', name: 'admin_pieces_add_parent', requirements: ['id' => '\d+'])]
    public function add(Breadcrumb $breadcrumb, ?int $id, SerieRepository $serieRepository): Response
    {
        $breadcrumb->add('Pièces', $this->generateUrl('admin_pieces'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_pieces_add'));
        $entity = (new Piece())->setSerie($serieRepository->find((int) $id));

        return $this->renderAdd('@admin/pieces/form.html.twig', $entity);
    }

    #[Route(path: '/pieces/edit-{id}', name: 'admin_pieces_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, PieceRepository $repository, int $id): Response
    {
        $breadcrumb->add('Pièces', $this->generateUrl('admin_pieces'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_pieces_edit', ['id' => $id]));

        return $this->renderEdit('@admin/pieces/form.html.twig', $repository->find($id));
    }

    #[Route(path: '/pieces/delete-{id}', name: 'admin_pieces_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, PieceRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_pieces');
    }

    #[Route(path: '/pieces/autocomplete', name: 'admin_pieces_autocomplete')]
    public function autocomplete(AutocompleteService $autocomplete, Request $request): Response
    {
        if ('images' === sf5Get($request, 'field_name')) {
            $result = $autocomplete->images($request, PieceType::class);
        } else {
            $result = $autocomplete->entities($request, PieceType::class);
        }

        return new JsonResponse($result);
    }
}
