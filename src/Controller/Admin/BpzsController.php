<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\BPZ;
use App\Form\AutocompleteService;
use App\Form\Type\Entity\BPZType;

use function App\Hack\Symfony5\sf5Get;

use App\Repository\BPZRepository;
use App\Repository\KinderRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BpzsController extends AdminAbstractController
{
    #[Route(path: '/bpzs/', name: 'admin_bpzs')]
    public function index(BPZRepository $repository, Breadcrumb $breadcrumb, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('BPZs', $this->generateUrl('admin_bpzs'));
        $searchFilter->setRouteName('admin_bpzs');

        return $this->render('@admin/bpzs/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/bpzs/add', name: 'admin_bpzs_add')]
    #[Route(path: '/bpzs/add-{id}', name: 'admin_bpzs_add_parent', requirements: ['id' => '\d+'])]
    public function add(Breadcrumb $breadcrumb, ?int $id, KinderRepository $kinderRepository): Response
    {
        $breadcrumb->add('BPZs', $this->generateUrl('admin_bpzs'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_bpzs_add'));
        if (empty($kinder = $kinderRepository->find((int) $id))) {
            return $this->goPrevious();
        }
        $entity = (new BPZ())->setKinder($kinder);

        return $this->renderAdd('@admin/bpzs/form.html.twig', $entity);
    }

    #[Route(path: '/bpzs/edit-{id}', name: 'admin_bpzs_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, BPZRepository $repository, int $id): Response
    {
        $breadcrumb->add('BPZs', $this->generateUrl('admin_bpzs'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_bpzs_edit', ['id' => $id]));

        return $this->renderEdit('@admin/bpzs/form.html.twig', $repository->find($id));
    }

    #[Route(path: '/bpzs/delete-{id}', name: 'admin_bpzs_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, BPZRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_bpzs');
    }

    #[Route(path: '/bpzs/autocomplete', name: 'admin_bpzs_autocomplete')]
    public function autocomplete(AutocompleteService $autocomplete, Request $request): Response
    {
        if ('images' === sf5Get($request, 'field_name')) {
            $result = $autocomplete->images($request, BPZType::class);
        } else {
            $result = $autocomplete->entities($request, BPZType::class);
        }

        return new JsonResponse($result);
    }
}
