<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\MenuItem;
use App\Repository\MenuItemRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MenuItemsController extends AdminAbstractController
{
    #[Route(path: '/menuitems/', name: 'admin_menuitems')]
    public function index(MenuItemRepository $repository, Breadcrumb $breadcrumb, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Menu item', $this->generateUrl('admin_menuitems'));

        return $this->render('@admin/menuitems/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/menuitems/add', name: 'admin_menuitems_add')]
    public function add(Breadcrumb $breadcrumb): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Menu item', $this->generateUrl('admin_menuitems'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_menuitems_add'));

        return $this->renderAdd('@admin/menuitems/form.html.twig', new MenuItem());
    }

    #[Route(path: '/menuitems/edit-{id}', name: 'admin_menuitems_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, MenuItemRepository $repository, int $id): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Menu item', $this->generateUrl('admin_menuitems'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_menuitems_edit', ['id' => $id]));

        return $this->renderEdit('@admin/menuitems/form.html.twig', $repository->find($id));
    }

    #[Route(path: '/menuitems/delete-{id}', name: 'admin_menuitems_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, MenuItemRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_menuitems');
    }
}
