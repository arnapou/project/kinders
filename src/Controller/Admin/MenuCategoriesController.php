<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\MenuCategory;
use App\Repository\MenuCategoryRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MenuCategoriesController extends AdminAbstractController
{
    #[Route(path: '/menucategs/', name: 'admin_menucategs')]
    public function index(
        MenuCategoryRepository $repository,
        Breadcrumb $breadcrumb,
        SearchFilter $searchFilter
    ): Response {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Menu catégorie', $this->generateUrl('admin_menucategs'));

        return $this->render('@admin/menucategs/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/menucategs/add', name: 'admin_menucategs_add')]
    public function add(Breadcrumb $breadcrumb): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Menu catégorie', $this->generateUrl('admin_menucategs'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_menucategs_add'));

        return $this->renderAdd('@admin/menucategs/form.html.twig', new MenuCategory());
    }

    #[Route(path: '/menucategs/edit-{id}', name: 'admin_menucategs_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, MenuCategoryRepository $repository, int $id): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Menu catégorie', $this->generateUrl('admin_menucategs'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_menucategs_edit', ['id' => $id]));

        return $this->renderEdit('@admin/menucategs/form.html.twig', $repository->find($id));
    }

    #[Route(path: '/menucategs/delete-{id}', name: 'admin_menucategs_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, MenuCategoryRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_menucategs');
    }
}
