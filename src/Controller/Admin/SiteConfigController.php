<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Repository\SiteConfigRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SiteConfigController extends AdminAbstractController
{
    #[Route(path: '/siteconfig/', name: 'admin_siteconfig')]
    public function index(
        SiteConfigRepository $repository,
        Breadcrumb $breadcrumb,
        SearchFilter $searchFilter
    ): Response {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Site', $this->generateUrl('admin_siteconfig'));

        return $this->render('@admin/siteconfig/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/siteconfig/edit-{id}', name: 'admin_siteconfig_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, SiteConfigRepository $repository, int $id): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Site', $this->generateUrl('admin_siteconfig'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_siteconfig_edit', ['id' => $id]));

        return $this->renderEdit('@admin/siteconfig/form.html.twig', $repository->find($id));
    }
}
