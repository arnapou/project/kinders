<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\BaseEntity;
use App\Form\FormFactory;
use App\Service\Admin\Breadcrumb;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class AdminAbstractController extends AbstractController
{
    public function __construct(
        private readonly Breadcrumb $breadcrumb,
        private readonly FormFactory $formFactory
    ) {
    }

    protected function goPrevious(): RedirectResponse
    {
        return $this->redirect($this->breadcrumb->previous());
    }

    /**
     * @param class-string|null $type
     */
    public function renderAdd(string $view, ?BaseEntity $entity, string $type = null): Response
    {
        $context = ['action' => 'Créer'];

        return $this->formFactory->render($view, $entity, [], $context, $type)
            ?: $this->goPrevious();
    }

    /**
     * @param class-string|null $type
     */
    protected function renderEdit(string $view, ?BaseEntity $entity, string $type = null): Response
    {
        $context = ['action' => 'Modifier'];

        return $this->formFactory->render($view, $entity, [], $context, $type)
            ?: $this->goPrevious();
    }
}
