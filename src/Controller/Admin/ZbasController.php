<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\ZBA;
use App\Form\AutocompleteService;
use App\Form\Type\Entity\ZBAType;

use function App\Hack\Symfony5\sf5Get;

use App\Repository\KinderRepository;
use App\Repository\ZBARepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ZbasController extends AdminAbstractController
{
    #[Route(path: '/zbas/', name: 'admin_zbas')]
    public function index(ZBARepository $repository, Breadcrumb $breadcrumb, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('ZBAs', $this->generateUrl('admin_zbas'));
        $searchFilter->setRouteName('admin_zbas');

        return $this->render('@admin/zbas/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/zbas/add', name: 'admin_zbas_add')]
    #[Route(path: '/zbas/add-{id}', name: 'admin_zbas_add_parent', requirements: ['id' => '\d+'])]
    public function add(Breadcrumb $breadcrumb, ?int $id, KinderRepository $kinderRepository): Response
    {
        $breadcrumb->add('ZBAs', $this->generateUrl('admin_zbas'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_zbas_add'));
        if (empty($kinder = $kinderRepository->find((int) $id))) {
            return $this->goPrevious();
        }
        $entity = (new ZBA())->setKinder($kinder);

        return $this->renderAdd('@admin/zbas/form.html.twig', $entity);
    }

    #[Route(path: '/zbas/edit-{id}', name: 'admin_zbas_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, ZBARepository $repository, int $id): Response
    {
        $breadcrumb->add('ZBAs', $this->generateUrl('admin_zbas'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_zbas_edit', ['id' => $id]));

        return $this->renderEdit('@admin/zbas/form.html.twig', $repository->find($id));
    }

    #[Route(path: '/zbas/delete-{id}', name: 'admin_zbas_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, ZBARepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_zbas');
    }

    #[Route(path: '/zbas/autocomplete', name: 'admin_zbas_autocomplete')]
    public function autocomplete(AutocompleteService $autocomplete, Request $request): Response
    {
        if ('images' === sf5Get($request, 'field_name')) {
            $result = $autocomplete->images($request, ZBAType::class);
        } else {
            $result = $autocomplete->entities($request, ZBAType::class);
        }

        return new JsonResponse($result);
    }
}
