<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Country;
use App\Repository\CountryRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CountriesController extends AdminAbstractController
{
    #[Route(path: '/countries/', name: 'admin_countries')]
    public function index(CountryRepository $repository, Breadcrumb $breadcrumb, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Pays', $this->generateUrl('admin_countries'));

        return $this->render('@admin/countries/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/countries/add', name: 'admin_countries_add')]
    public function add(Breadcrumb $breadcrumb): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Pays', $this->generateUrl('admin_countries'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_countries_add'));

        return $this->renderAdd('@admin/countries/form.html.twig', new Country());
    }

    #[Route(path: '/countries/edit-{id}', name: 'admin_countries_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, CountryRepository $repository, int $id): Response
    {
        $breadcrumb->add('Config', '');
        $breadcrumb->add('Pays', $this->generateUrl('admin_countries'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_countries_edit', ['id' => $id]));

        return $this->renderEdit('@admin/countries/form.html.twig', $repository->find($id));
    }

    #[Route(path: '/countries/delete-{id}', name: 'admin_countries_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, CountryRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_countries');
    }
}
