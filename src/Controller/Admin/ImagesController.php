<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Assert;
use App\Entity\Image;
use App\Form\Type\Multiple\ImagesUploadType;
use App\Repository\ImageRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;

use function in_array;

use InvalidArgumentException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImagesController extends AdminAbstractController
{
    #[Route(path: '/images/', name: 'admin_images')]
    public function index(Breadcrumb $breadcrumb, ImageRepository $repository, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('Images', $this->generateUrl('admin_images'));
        $searchFilter->setRouteName('admin_images');

        return $this->render('@admin/images/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/images/add', name: 'admin_images_add')]
    #[Route(path: '/images/add-{type}', name: 'admin_images_add_type')]
    public function add(
        Breadcrumb $breadcrumb,
        Request $request,
        EntityManagerInterface $entityManager,
        ImageRepository $imageRepository,
        ?string $type,
        FormFactoryInterface $formFactory
    ): Response {
        $breadcrumb->add('Images', $this->generateUrl('admin_images'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_images_add'));
        $data = ['type' => in_array($type, $imageRepository->getTypes(), true) ? $type : ''];
        $form = $formFactory->create(ImagesUploadType::class, $data, [
            'image_type' => $data['type'],
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = Assert::array($form->getData());
            if (!in_array($data['type'], $imageRepository->getTypes(), true)) {
                throw new InvalidArgumentException('Not allowed image type');
            }
            for ($n = 1; $n <= ImagesUploadType::NB_IMAGES; ++$n) {
                /** @var Image $image */
                if ($image = $data["image$n"]) {
                    $image->setType($data['type']);
                    $entityManager->persist($image);
                }
            }
            $entityManager->flush();

            return $this->redirect($breadcrumb->previous());
        }

        return $this->render('@admin/images/add.html.twig', [
            'form' => $form->createView(),
            'nbimages' => ImagesUploadType::NB_IMAGES,
        ]);
    }

    #[Route(path: '/images/delete-{id}', name: 'admin_images_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, ImageRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_images');
    }

    #[Route(path: '/images/edit-{id}', name: 'admin_images_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, ImageRepository $repository, int $id): Response
    {
        $breadcrumb->add('Images', $this->generateUrl('admin_images'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_images_edit', ['id' => $id]));

        return $this->renderEdit('@admin/images/edit.html.twig', $repository->find($id));
    }
}
