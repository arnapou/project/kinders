<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Kinder;
use App\Form\AutocompleteService;
use App\Form\Type\Entity\KinderType;
use App\Form\Type\Entity\KinderVirtualType;

use function App\Hack\Symfony5\sf5Get;

use App\Repository\KinderRepository;
use App\Repository\SerieRepository;
use App\Service\Admin\Breadcrumb;
use App\Service\Admin\SearchFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class KindersController extends AdminAbstractController
{
    #[Route(path: '/kinders/', name: 'admin_kinders')]
    public function index(KinderRepository $repository, Breadcrumb $breadcrumb, SearchFilter $searchFilter): Response
    {
        $breadcrumb->add('Kinders', $this->generateUrl('admin_kinders'));
        $searchFilter->setRouteName('admin_kinders');

        return $this->render('@admin/kinders/index.html.twig', [
            'items' => $searchFilter->search($repository),
        ]);
    }

    #[Route(path: '/kinders/add', name: 'admin_kinders_add')]
    #[Route(path: '/kinders/add-{id}', name: 'admin_kinders_add_parent', requirements: ['id' => '\d+'])]
    public function add(Breadcrumb $breadcrumb, ?int $id, SerieRepository $serieRepository): Response
    {
        $breadcrumb->add('Kinders', $this->generateUrl('admin_kinders'));
        $breadcrumb->add('Ajouter', $this->generateUrl('admin_kinders_add'));
        if (empty($serie = $serieRepository->find((int) $id))) {
            return $this->goPrevious();
        }
        $entity = (new Kinder())->setSerie($serie);

        return $this->renderAdd('@admin/kinders/form.html.twig', $entity);
    }

    #[Route(path: '/kinders/edit-{id}', name: 'admin_kinders_edit', requirements: ['id' => '\d+'])]
    public function edit(Breadcrumb $breadcrumb, KinderRepository $repository, int $id): Response
    {
        $breadcrumb->add('Kinders', $this->generateUrl('admin_kinders'));
        $breadcrumb->add('Modifier', $this->generateUrl('admin_kinders_edit', ['id' => $id]));
        $kinder = $repository->find($id);
        if ($kinder && $kinder->isVirtual()) {
            return $this->renderEdit('@admin/kinders/form_virtual.html.twig', $kinder, KinderVirtualType::class);
        }

        return $this->renderEdit('@admin/kinders/form.html.twig', $kinder);
    }

    #[Route(path: '/kinders/delete-{id}', name: 'admin_kinders_delete', requirements: ['id' => '\d+'], methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, KinderRepository $repository, int $id): Response
    {
        if ($item = $repository->find($id)) {
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_kinders');
    }

    #[Route(path: '/kinders/autocomplete', name: 'admin_kinders_autocomplete')]
    public function autocomplete(AutocompleteService $autocomplete, Request $request): Response
    {
        if ('images' === sf5Get($request, 'field_name')) {
            $result = $autocomplete->images($request, KinderType::class);
        } else {
            $result = $autocomplete->entities($request, KinderType::class);
        }

        return new JsonResponse($result);
    }
}
