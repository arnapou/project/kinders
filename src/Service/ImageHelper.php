<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service;

use App\Assert;
use App\Entity\Image;
use App\Form\VichStorage;

use function in_array;
use function is_string;
use function strlen;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UnexpectedValueException;

/**
 * Remplit la fonction de UploaderHelper pour pouvoir gérer le fieldName par défaut
 * et centraliser d'autres mécaniques utiles.
 *
 * @see \Vich\UploaderBundle\Templating\Helper\UploaderHelper
 */
class ImageHelper
{
    private readonly string $uploadDestination;

    public function __construct(
        protected VichStorage $storage,
        ParameterBagInterface $parameterBag
    ) {
        $this->uploadDestination = $this->validateUploadDestination(
            Assert::string($parameterBag->get('vich.upload_destination'))
        );
    }

    private function validateUploadDestination(array|bool|string|int|float|null $value): string
    {
        return is_string($value) ? $value
            : throw new UnexpectedValueException('Parameter vich.upload_destination is not a string.');
    }

    public function asset(Image $obj): string
    {
        return $this->storage->resolveUri($obj, Image::VICH_FIELD, null)
            ?? throw new UnexpectedValueException('Asset should not be null');
    }

    public function getUploadDestination(): string
    {
        return $this->uploadDestination;
    }

    public function thumbnail(Image|string|null $filename, int $w = 0, int $h = 0): ?string
    {
        if ($filename instanceof Image) {
            $filename = $this->asset($filename);
        }

        if (empty($filename)) {
            return null;
        }

        $infos = pathinfo($filename);
        if (empty($infos['extension'])) {
            return null;
        }

        if ($w && $h) {
            $resize = ".{$w}x{$h}";
        } elseif ($w) {
            $resize = ".{$w}";
        } elseif ($h) {
            $resize = ".x{$h}";
        } else {
            $resize = '';
        }

        return $infos['dirname'] . '/' . $infos['filename'] . '_tn' . $resize . '.' . $infos['extension'];
    }

    /**
     * @return array{path: string, ext: string}|null
     */
    public function thumbnailRouteParameters(?Image $image): ?array
    {
        if (null === $image) {
            return null;
        }

        $path = $this->asset($image);
        if (str_starts_with($path, Image::PUBLIC_DIR)) {
            $path = substr($path, strlen(Image::PUBLIC_DIR));
        }
        $infos = pathinfo(ltrim($path, '/'));
        $path = $infos['extension'] ?? null;

        if (!isset($infos['dirname'])
            || !in_array($path, Image::EXTENSIONS, true)
        ) {
            throw new NotFoundHttpException('Not Found');
        }

        return [
            'path' => $infos['dirname'] . '/' . $infos['filename'],
            'ext' => $path,
        ];
    }
}
