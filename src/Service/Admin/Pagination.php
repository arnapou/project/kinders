<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service\Admin;

use App\Assert;

use function App\Hack\Symfony5\sf5Get;

use ArrayIterator;

use function count;

use IteratorAggregate;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @implements IteratorAggregate<int, int>
 */
class Pagination implements IteratorAggregate
{
    final public const MAX_PAGES = 10;
    private ?int $pageNum = null;
    private readonly int $pageSize;
    private int $pageCount = 1;
    private int $itemCount = 0;

    public function __construct(
        private readonly RequestStack $requestStack,
        ParameterBagInterface $parameterBag
    ) {
        $this->pageSize = Assert::int($parameterBag->get('admin.pagination.page_size') ?: 15);
    }

    public function setItemCount(int $count): void
    {
        $this->itemCount = $count;
        $this->pageCount = (int) ceil($count / $this->pageSize);
    }

    public function getPageNum(): int
    {
        if (null === $this->pageNum) {
            if ($request = $this->requestStack->getCurrentRequest()) {
                $num = Assert::string(sf5Get($request, 'page'));
                $routeName = Assert::string(sf5Get($request, '_route'));
                if ($num && ctype_digit($num)) {
                    $request->getSession()->set("page.$routeName", (int) $num);
                } else {
                    $num = Assert::int($request->getSession()->get("page.$routeName"));
                }
                if ($num < 1) {
                    $num = 1;
                }
                if ($num > $this->pageCount) {
                    $num = $this->pageCount;
                }
            } else {
                $num = 1;
            }
            $this->pageNum = (int) $num;
        }

        return $this->pageNum;
    }

    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    public function getPageCount(): int
    {
        return $this->pageCount;
    }

    public function getItemCount(): int
    {
        return $this->itemCount;
    }

    public function offsetStart(): int
    {
        $offset = ($this->getPageNum() - 1) * $this->getPageSize();

        return max($offset, 0);
    }

    /**
     * @return ArrayIterator<int, int>
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->getPages());
    }

    /**
     * @return array<int, int>
     */
    public function getPages(): array
    {
        $page = $this->getPageNum();
        $count = $this->getPageCount();

        if ($count <= self::MAX_PAGES) {
            return range(1, $count);
        }

        $half = (int) floor(self::MAX_PAGES / 2);
        $pages = range($page - $half + 1, $page + $half);
        if ($pages[0] < 1) {
            $val = 1 - $pages[0];
            foreach ($pages as &$page) {
                $page += $val;
            }
        }

        if ($pages[count($pages) - 1] >= $count) {
            $val = $pages[count($pages) - 1] - $count;
            foreach ($pages as &$page) {
                $page -= $val;
            }
        }

        $pages = array_unique(array_merge($pages, [1, $count]));
        sort($pages);
        $newPages = [$pages[0]];
        for ($i = 1, $iMax = count($pages); $i < $iMax; ++$i) {
            if ($pages[$i] - $pages[$i - 1] > 1) {
                $newPages[] = 0;
            }
            $newPages[] = $pages[$i];
        }

        return $newPages;
    }
}
