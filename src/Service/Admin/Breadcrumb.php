<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service\Admin;

use ArrayIterator;

use function count;

use Countable;

use function is_array;

use IteratorAggregate;
use Stringable;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

/**
 * @phpstan-type BreadcrumbItem array{ label: string, url: string }
 *
 * @implements IteratorAggregate<int, BreadcrumbItem>
 */
class Breadcrumb implements IteratorAggregate, Countable, Stringable
{
    /**
     * @var array<int, BreadcrumbItem>
     */
    private array $items = [];

    /**
     * Breadcrumb constructor.
     */
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly RouterInterface $router
    ) {
    }

    /**
     * @return BreadcrumbItem|null
     */
    public function getFirst(): ?array
    {
        return $this->items[0] ?? null;
    }

    public function add(string $label, string $url): void
    {
        $this->items[] = [
            'label' => $label,
            'url' => $url,
        ];
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function __toString(): string
    {
        return implode(
            ' / ',
            array_map(static fn ($item): string => $item['label'], $this->items)
        );
    }

    public function previous(): string
    {
        $request = $this->requestStack->getCurrentRequest();
        if (($previous = $request?->getSession()->get('last_route'))
            && is_array($previous)
            && isset($previous['name'], $previous['params'])
        ) {
            return $this->router->generate($previous['name'], $previous['params']);
        }

        return ($this->items[0] ?? $this->items[1] ?? ['url' => './'])['url'];
    }
}
