<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service;

use App\Entity\Image;

use function is_object;
use function is_scalar;

use Stringable;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    public function __construct(
        private readonly ImageHelper $helper
    ) {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('tn', [$this, 'thumbnail']),
            new TwigFilter('href', [$this, 'href']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('vich_uploader_asset', [$this, 'vich_uploader_asset']),
        ];
    }

    public function href(mixed $object): string
    {
        return match (true) {
            $object instanceof Image => $this->helper->asset($object),
            $object instanceof Stringable,
            is_scalar($object),
            is_object($object) && method_exists($object, '__toString') => (string) $object,
            default => '',
        };
    }

    public function thumbnail(Image|string|null $filename, int $w = 0, int $h = 0): ?string
    {
        return $this->helper->thumbnail($filename, $w, $h);
    }

    /**
     * @deprecated présent que pour comaptibilité avec l'extension native
     */
    public function vich_uploader_asset(Image $object): ?string
    {
        return $this->helper->asset($object);
    }
}
