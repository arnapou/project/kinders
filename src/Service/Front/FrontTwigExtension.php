<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service\Front;

use App\Entity\Kinder;
use App\Entity\Serie;
use App\Repository\CollectionRepository;
use App\Repository\MenuItemRepository;
use App\Repository\SerieRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FrontTwigExtension extends AbstractExtension
{
    public function __construct(
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly SerieRepository $serieRepository,
        private readonly CollectionRepository $collectionRepository,
        private readonly MenuItemRepository $menuItemRepository,
    ) {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('kinderRefs', [$this, 'kinderReferences']),
            new TwigFilter('internalLinks', [$this, 'internalLinks'], ['is_safe' => ['html']]),
        ];
    }

    public function internalLinks(string $text): string
    {
        return (string) preg_replace_callback(
            '!\{\{\s*(\w+)\s+(\d+)\s*\}\}!s',
            function (array $matches): string {
                [, $type, $value] = $matches;

                return match (strtolower(trim($type))) {
                    'serie' => '<a href="' . $this->urlGenerator->generate('front_serie', ['id' => (int) $value]) . '">'
                        . $this->serieRepository->find((int) $value)?->getName()
                        . '</a>',
                    'collection' => '<a href="' . $this->urlGenerator->generate('front_collection', ['id' => (int) $value]) . '">'
                        . $this->collectionRepository->find((int) $value)?->getName()
                        . '</a>',
                    'search' => '<a href="' . $this->urlGenerator->generate('front_search', ['id' => (int) $value]) . '">'
                        . $this->menuItemRepository->find((int) $value)?->getName()
                        . '</a>',
                    default => ''
                };
            },
            $text
        );
    }

    public function kinderReferences(Serie $serie): array
    {
        $refs = [];
        foreach ($serie->getKinders() as $kinder) {
            if ($kinder instanceof Kinder
                && $kinder->getReference()
                && !isset($refs[$kinder->getReference()])
            ) {
                $refs[$kinder->getReference()] = $kinder->getName();
            }
        }

        return $refs;
    }
}
