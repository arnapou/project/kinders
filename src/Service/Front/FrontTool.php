<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service\Front;

use App\Assert;
use App\Entity\BPZ;
use App\Entity\Country;
use App\Entity\Kinder;
use App\Entity\Serie;
use App\Entity\ZBA;
use App\Presenter\KinderPresenter;
use App\Presenter\SeriePresenter;

use function array_key_exists;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

class FrontTool
{
    /** @var array<string, mixed> */
    private array $memory = [];

    public function __construct(
        private readonly CacheItemPoolInterface $cache,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function cached(string $key, callable $factory, int $ttl = 15): mixed
    {
        if (array_key_exists($key, $this->memory)) {
            return $this->memory[$key];
        }

        /** @var CacheItemInterface $item $item */
        $item = $this->cache->getItem($key);

        if (!$item->isHit()) {
            $item->set($factory());
            $item->expiresAfter($ttl);
            $this->cache->save($item);
        }

        return $this->memory[$key] = $item->get();
    }

    /**
     * Défini un item country à la main pour économiser doctrine.
     *
     * @param array<int, SeriePresenter> $series
     */
    public function populateCountry(array $series): void
    {
        $countries = $this->queryCountries();

        foreach ($series as $serie) {
            if ($id = $serie->getCountry()?->getId()) {
                $serie->country = $countries[$id];
            }
        }
    }

    /**
     * Définit la première image de la série.
     *
     * @param array<int, SeriePresenter> $series
     * @param array<int, Kinder>         $kinders
     */
    public function populateImage(array $series, array $kinders): void
    {
        $images = $this->queryKinderImages(array_keys($kinders));

        foreach ($images as $kinder) {
            if ($id = $kinder->getSerie()?->getId()) {
                if (null !== $series[$id]->image) {
                    continue;
                }
                if ($image = $kinder->getImage()) {
                    $series[$id]->image = $image;
                }
            }
        }
    }

    /**
     * Une série est considérée complete si on n'a aucun champ lookingFor à TRUE pour aucun Kinder, BPZ ou ZBA de la série.
     *
     * @param array<int, SeriePresenter> $series
     * @param array<int, Kinder>         $kinders
     */
    public function populateComplete(array $series, array $kinders): void
    {
        $kinderIds = array_keys($kinders);
        $bpzs = $this->queryBpzs($kinderIds);
        $zbas = $this->queryZbas($kinderIds);

        foreach ($kinders as $kinder) {
            if ($idSerie = $kinder->getSerie()?->getId()) {
                $series[$idSerie]->complete = $series[$idSerie]->complete && !$kinder->isLookingFor();
            }
        }

        foreach ($bpzs as $bpz) {
            if ($kinderId = $bpz->getKinder()?->getId()) {
                $kinder = $kinders[$kinderId];
                if ($idSerie = $kinder->getSerie()?->getId()) {
                    $series[$idSerie]->complete = $series[$idSerie]->complete && !$bpz->isLookingFor();
                }
            }
        }

        foreach ($zbas as $zba) {
            if ($kinderId = $zba->getKinder()?->getId()) {
                $kinder = $kinders[$kinderId];
                if ($idSerie = $kinder->getSerie()?->getId()) {
                    $series[$idSerie]->complete = $series[$idSerie]->complete && !$zba->isLookingFor();
                }
            }
        }
    }

    /**
     * Remplace les kinders par la liste réelle en direct pour économiser des requêtes.
     *
     * @param array<int, SeriePresenter>         $series
     * @param array<int, Kinder|KinderPresenter> $kinders
     */
    public function populateKinders(array $series, array $kinders): void
    {
        foreach ($kinders as $id => $kinder) {
            $serieId = $kinder->getSerie()?->getId();
            if (!$serieId || !isset($series[$serieId])) {
                continue;
            }
            $series[$serieId]->kinders[$id] = $kinder;
        }

        $sortKinders = static function (object $a, object $b): int {
            if ($a instanceof KinderPresenter && $b instanceof KinderPresenter) {
                foreach (Serie::KINDER_SORTING as $field => $order) {
                    $asc = 'ASC' === strtoupper($order);
                    $res = $a->$field() <=> $b->$field();
                    if (0 !== $res) {
                        /* @phpstan-ignore-next-line */
                        return $asc ? $res : -$res;
                    }
                }
            }

            return 0;
        };

        foreach ($series as $serie) {
            uasort($serie->kinders, $sortKinders);
        }
    }

    /**
     * @return array<int, Country>
     */
    private function queryCountries(): array
    {
        return Assert::arrayOf($this->entityManager->createQueryBuilder()
            ->select('e')
            ->from(Country::class, 'e', 'e.id')
            ->getQuery()->getResult(), Country::class);
    }

    /**
     * @param int[] $kinderIds
     *
     * @return array<int, Kinder>
     */
    private function queryKinderImages(array $kinderIds): array
    {
        if (!$kinderIds) {
            return [];
        }

        // Kinders en direct indexés par ID
        $qb1 = $this->entityManager->createQueryBuilder()
            ->select('e, i')
            ->from(Kinder::class, 'e', 'e.id')
            ->join('e.images', 'i')
            ->where('e.id IN (:ids)')
            ->setParameter('ids', $kinderIds);

        foreach (Serie::KINDER_SORTING as $field => $order) {
            $qb1->addOrderBy("e.$field", $order);
        }

        // Kinders originaux (non-virtuels) indexés par ID
        $qb2 = $this->entityManager->createQueryBuilder()
            ->select('e, o, i')
            ->from(Kinder::class, 'e', 'e.id')
            ->join('e.original', 'o')
            ->join('o.images', 'i')
            ->where('e.id IN (:ids)')
            ->setParameter('ids', $kinderIds);

        foreach (Serie::KINDER_SORTING as $field => $order) {
            $qb2->addOrderBy("e.$field", $order);
        }

        // le + est voulu -> ajoute les id manquants
        return $qb1->getQuery()->getResult() + $qb2->getQuery()->getResult();
    }

    /**
     * @param int[] $serieIds
     *
     * @return array<int, Kinder>
     */
    public function queryAllKinders(array $serieIds): array
    {
        if (!$serieIds) {
            return [];
        }

        $qb = $this->entityManager->createQueryBuilder()
            ->select('e')
            ->from(Kinder::class, 'e', 'e.id')
            ->where('e.serie IN (:ids)')
            ->setParameter('ids', $serieIds);

        foreach (Serie::KINDER_SORTING as $field => $order) {
            $qb->addOrderBy("e.$field", $order);
        }

        return Assert::arrayOf($qb->getQuery()->getResult(), Kinder::class);
    }

    /**
     * @param int[] $kinderIds
     *
     * @return array<int, ZBA>
     */
    private function queryZbas(array $kinderIds): array
    {
        if (!$kinderIds) {
            return [];
        }

        return Assert::arrayOf($this->entityManager->createQueryBuilder()
            ->select('e')
            ->from(ZBA::class, 'e', 'e.id')
            ->where('e.kinder IN (:ids)')
            ->setParameter('ids', $kinderIds)
            ->getQuery()->getResult(), ZBA::class);
    }

    /**
     * @param int[] $kinderIds
     *
     * @return array<int, BPZ>
     */
    private function queryBpzs(array $kinderIds): array
    {
        if (!$kinderIds) {
            return [];
        }

        return Assert::arrayOf($this->entityManager->createQueryBuilder()
            ->select('e')
            ->from(BPZ::class, 'e', 'e.id')
            ->where('e.kinder IN (:ids)')
            ->setParameter('ids', $kinderIds)
            ->getQuery()->getResult(), BPZ::class);
    }
}
