<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service\Front;

use App\Assert;
use App\Entity\BPZ;
use App\Entity\Kinder;
use App\Entity\ZBA;

class PageDoubles extends PageLookingFor
{
    protected function increment(null|Kinder|ZBA|BPZ $kinder): int
    {
        return $kinder ? $kinder->getQuantityDouble() : 0;
    }

    protected function queryKinders(): array
    {
        return Assert::arrayOf(
            $this->entityManager->createQueryBuilder()
                ->select('k')
                ->from(Kinder::class, 'k')
                ->join('k.serie', 's')
                ->andWhere('k.quantityDouble > 0')
                ->getQuery()->getResult(),
            Kinder::class
        );
    }

    protected function queryBpzs(): array
    {
        return Assert::arrayOf(
            $this->entityManager->createQueryBuilder()
                ->select('e, k')
                ->from(BPZ::class, 'e')
                ->join('e.kinder', 'k')
                ->join('k.serie', 's')
                ->andWhere('e.quantityDouble > 0')
                ->getQuery()->getResult(),
            BPZ::class
        );
    }

    protected function queryZbas(): array
    {
        return Assert::arrayOf(
            $this->entityManager->createQueryBuilder()
                ->select('e, k')
                ->from(ZBA::class, 'e')
                ->join('e.kinder', 'k')
                ->join('k.serie', 's')
                ->andWhere('e.quantityDouble > 0')
                ->getQuery()->getResult(),
            ZBA::class
        );
    }
}
