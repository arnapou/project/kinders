<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service\Front;

use App\Assert;
use App\Entity\BPZ;
use App\Entity\Kinder;
use App\Entity\Serie;
use App\Entity\ZBA;
use App\Presenter\KinderPresenter;
use App\Presenter\SeriePresenter;
use Doctrine\ORM\EntityManagerInterface;
use Error;

class PageLookingFor
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected FrontTool $tool
    ) {
    }

    public function getSeries(): array
    {
        $stats = $this->queryStats(
            $realKinders = $this->queryKinders(),
            $realBpzs = $this->queryBpzs(),
            $realZbas = $this->queryZbas(),
        );

        $qb = $this->entityManager->createQueryBuilder()
            ->select('s')
            ->from(Serie::class, 's', 's.id')
            ->join('s.country', 'c')
            ->andWhere('s.id IN (:ids)')
            ->setParameter(':ids', array_keys($stats))
            ->addOrderBy('s.year', 'DESC')
            ->addOrderBy('c.sorting', 'ASC')
            ->addOrderBy('c.name', 'ASC')
            ->addOrderBy('s.name', 'ASC');

        $series = array_map(
            static fn (mixed $s) => match (true) {
                $s instanceof Serie => new SeriePresenter($s),
                default => throw new Error()
            },
            Assert::array($qb->getQuery()->getResult())
        );
        $kinders = $this->tool->queryAllKinders(array_keys($series));

        $this->tool->populateCountry($series);
        $this->tool->populateImage($series, $kinders);
        $this->tool->populateKinders($series, $this->getKindersPresenter($realKinders, $realBpzs, $realZbas));

        foreach ($series as $serie) {
            if ($id = $serie->getId()) {
                $serie->stats['kinder'] += $stats[$id]['kinder'] ?? 0;
                $serie->stats['bpz'] += $stats[$id]['bpz'] ?? 0;
                $serie->stats['zba'] += $stats[$id]['zba'] ?? 0;
            }
        }

        return $series;
    }

    /**
     * @param array<Kinder> $kinders
     * @param array<BPZ>    $bpzs
     * @param array<ZBA>    $zbas
     *
     * @return array<int, KinderPresenter>
     */
    private function getKindersPresenter(array $kinders, array $bpzs, array $zbas): array
    {
        $items = [];

        foreach ($kinders as $item) {
            if ($id = $item->getId()) {
                $items[$id] ??= new KinderPresenter($item);
                $items[$id]->flag['kinder'] = true;
            }
        }

        foreach ($bpzs as $item) {
            if ($id = $item->getKinder()?->getId()) {
                $items[$id] ??= new KinderPresenter($item->getKinder());
                $items[$id]->flag['bpz'] = true;
            }
        }

        foreach ($zbas as $item) {
            if ($id = $item->getKinder()?->getId()) {
                $items[$id] ??= new KinderPresenter($item->getKinder());
                $items[$id]->flag['zba'] = true;
            }
        }

        return $items;
    }

    /**
     * @param array<Kinder> $kinders
     * @param array<BPZ>    $bpzs
     * @param array<ZBA>    $zbas
     *
     * @return array<int, array{kinder?: int, zba?: int, bpz?: int}>
     */
    private function queryStats(array $kinders, array $bpzs, array $zbas): array
    {
        $stats = [];

        foreach ($kinders as $item) {
            if ($id = $item->getSerie()?->getId()) {
                $stats[$id]['kinder'] = ($stats[$id]['kinder'] ?? 0) + $this->increment($item);
            }
        }

        foreach ($bpzs as $item) {
            if ($id = $item->getKinder()?->getSerie()?->getId()) {
                $stats[$id]['bpz'] = ($stats[$id]['bpz'] ?? 0) + $this->increment($item);
            }
        }

        foreach ($zbas as $item) {
            if ($id = $item->getKinder()?->getSerie()?->getId()) {
                $stats[$id]['zba'] = ($stats[$id]['zba'] ?? 0) + $this->increment($item);
            }
        }

        return $stats;
    }

    protected function increment(null|Kinder|BPZ|ZBA $kinder): int
    {
        return $kinder ? 1 : 0;
    }

    /**
     * @return array<int, Kinder>
     */
    protected function queryKinders(): array
    {
        return Assert::arrayOf(
            $this->entityManager->createQueryBuilder()
                ->select('k')
                ->from(Kinder::class, 'k')
                ->join('k.serie', 's')
                ->andWhere('k.lookingFor = true')
                ->getQuery()->getResult(),
            Kinder::class
        );
    }

    /**
     * @return array<int, BPZ>
     */
    protected function queryBpzs(): array
    {
        return Assert::arrayOf(
            $this->entityManager->createQueryBuilder()
                ->select('e, k')
                ->from(BPZ::class, 'e')
                ->join('e.kinder', 'k')
                ->join('k.serie', 's')
                ->andWhere('e.lookingFor = true')
                ->getQuery()->getResult(),
            BPZ::class
        );
    }

    /**
     * @return array<int, ZBA>
     */
    protected function queryZbas(): array
    {
        return Assert::arrayOf(
            $this->entityManager->createQueryBuilder()
                ->select('e, k')
                ->from(ZBA::class, 'e')
                ->join('e.kinder', 'k')
                ->join('k.serie', 's')
                ->andWhere('e.lookingFor = true')
                ->getQuery()->getResult(),
            ZBA::class
        );
    }
}
