<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Kinder;
use App\Entity\Serie;
use App\Exception\KinderVirtualException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Serie>
 */
class SerieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Serie::class);
    }

    public function copyVirtual(?Serie $from, ?Serie $to): void
    {
        if (!$from || !$to) {
            return;
        }
        $alreadyVirtuals = [];
        foreach ($to->getKinders() as $kinder) {
            if ($kinder instanceof Kinder
                && $kinder->isVirtual()
                && ($originalId = $kinder->getOriginal()?->getId())
            ) {
                $alreadyVirtuals[$originalId] = true;
            }
        }
        $em = $this->getEntityManager();
        foreach ($from->getKinders() as $kinder) {
            if (!$kinder instanceof Kinder) {
                continue;
            }
            $kinder = ($kinder->isVirtual() ? $kinder->getOriginal() : $kinder) ?? $kinder;
            if (($id = $kinder->getId()) && !isset($alreadyVirtuals[$id])) {
                try {
                    $newKinder = new Kinder();
                    $newKinder->setName($kinder->getName());
                    $newKinder->setSerie($to);
                    $newKinder->setOriginal($kinder);
                    $em->persist($newKinder);
                } catch (KinderVirtualException) {
                }
            }
        }
        $em->flush();
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        return parent::findBy($criteria, $orderBy ?: ['name' => 'ASC'], $limit, $offset);
    }
}
