<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Assert;
use App\Entity\BaseItem;
use App\Entity\BPZ;
use App\Entity\Image;
use App\Entity\Item;
use App\Entity\Kinder;
use App\Entity\Piece;
use App\Entity\Serie;
use App\Entity\ZBA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use ReflectionClass;

/**
 * @extends ServiceEntityRepository<Image>
 */
class ImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Image::class);
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        return parent::findBy($criteria, $orderBy ?: ['type' => 'ASC', 'name' => 'ASC'], $limit, $offset);
    }

    public function getTypes(): array
    {
        $types = [];
        $em = $this->getEntityManager();
        $meta = $em->getMetadataFactory()->getAllMetadata();
        foreach ($meta as $m) {
            $reflectionClass = new ReflectionClass($m->getName());
            if ($reflectionClass->isInstantiable() && $reflectionClass->isSubclassOf(BaseItem::class)) {
                $types[] = $reflectionClass->getShortName();
            }
        }

        return array_combine($types, $types);
    }

    public function linked(?Image $image): bool
    {
        if (null === $image) {
            return false;
        }
        $classes = [
            BPZ::class,
            ZBA::class,
            Kinder::class,
            Piece::class,
            Item::class,
            Serie::class,
        ];
        foreach ($classes as $class) {
            if ($objects = $this->linkedObjects($image, $class)) {
                return true;
            }
        }

        return false;
    }

    public function linkedObjects(Image $image, string $class): array
    {
        return Assert::array(
            $this
                ->getEntityManager()
                ->createQueryBuilder()
                ->from($class, 'o')
                ->select('o')
                ->join('o.images', 'i', Join::WITH, 'i.id = :id')
                ->setParameter('id', $image->getId())
                ->getQuery()->getResult()
        );
    }

    /**
     * @return BPZ[]
     */
    public function linkedBPZ(Image $image): array
    {
        return $this->linkedObjects($image, BPZ::class);
    }

    /**
     * @return ZBA[]
     */
    public function linkedZBA(Image $image): array
    {
        return $this->linkedObjects($image, ZBA::class);
    }

    /**
     * @return Kinder[]
     */
    public function linkedKinder(Image $image): array
    {
        return $this->linkedObjects($image, Kinder::class);
    }

    /**
     * @return Piece[]
     */
    public function linkedPiece(Image $image): array
    {
        return $this->linkedObjects($image, Piece::class);
    }

    /**
     * @return Serie[]
     */
    public function linkedSerie(Image $image): array
    {
        return $this->linkedObjects($image, Serie::class);
    }

    /**
     * @return Item[]
     */
    public function linkedItem(Image $image): array
    {
        return $this->linkedObjects($image, Item::class);
    }

    /**
     * @param class-string|object $class
     */
    public static function getTypeFrom(object|string $class): string
    {
        return (new ReflectionClass($class))->getShortName();
    }
}
