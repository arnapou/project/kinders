<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use App\Assert;
use App\Entity\AdminUser;
use App\Repository\AdminUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AdminUserCreateCommand extends Command
{
    public function __construct(
        protected AdminUserRepository $repository,
        protected EntityManagerInterface $entityManager,
        protected UserPasswordHasherInterface $encoder
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('admin:user:create')
            ->setDescription('Créer ou modifier un admin')
            ->addArgument('username', InputArgument::REQUIRED, 'username');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $question = new Question('Password: ');
        $question->setHidden(true);
        $password = Assert::string($io->askQuestion($question));
        $username = Assert::string($input->getArgument('username'));

        $admin = $this->repository->findOneBy(['username' => $username]);
        if (!$admin instanceof AdminUser) {
            $admin = new AdminUser();
        }
        $admin->setUsername($username);
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPassword($this->encoder->hashPassword($admin, $password));
        $created = $admin->getId();

        $this->entityManager->persist($admin);
        $this->entityManager->flush();
        $output->writeln('Utilisateur ' . ($created ? 'modifié' : 'créé') . ' !');

        $output->writeln('');
        foreach ($this->repository->findAll() as $adminUser) {
            $output->writeln('<info>#' . $adminUser->getId() . '</info> ' . $adminUser->getUserIdentifier());
        }

        return 0;
    }
}
