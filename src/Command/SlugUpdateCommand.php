<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use App\Entity\Attribute;
use App\Entity\BPZ;
use App\Entity\Collection;
use App\Entity\Country;
use App\Entity\Image;
use App\Entity\Item;
use App\Entity\Kinder;
use App\Entity\Piece;
use App\Entity\Serie;
use App\Entity\ZBA;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SlugUpdateCommand extends Command
{
    public function __construct(
        protected EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('admin:slug:update')
            ->setDescription('Met à jour tous les slugs');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $classes = [
            Attribute::class,
            BPZ::class,
            Collection::class,
            Country::class,
            Image::class,
            Item::class,
            Kinder::class,
            Piece::class,
            Serie::class,
            ZBA::class,
        ];

        foreach ($classes as $class) {
            foreach ($this->entityManager->getRepository($class)->findAll() as $obj) {
                $obj->updateSlug();
                $this->entityManager->persist($obj);
            }
            $this->entityManager->flush();
        }

        return 0;
    }
}
