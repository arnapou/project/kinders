<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: \App\Repository\ItemRepository::class)]
#[ORM\Index(columns: ['created_at'], name: 'created_at')]
#[ORM\Index(columns: ['updated_at'], name: 'updated_at')]
#[ORM\Index(columns: ['realsorting'], name: 'realsorting')]
#[ORM\Index(columns: ['slug'], name: 'slug')]
#[ORM\Index(columns: ['name'], name: 'name')]
#[ORM\Index(columns: ['quantity_owned'], name: 'quantity_owned')]
#[ORM\Index(columns: ['quantity_double'], name: 'quantity_double')]
#[ORM\Index(columns: ['reference'], name: 'reference')]
#[ORM\Index(columns: ['looking_for'], name: 'looking_for')]
#[ORM\Index(columns: ['year'], name: 'year')]
class Item extends BaseItem
{
    #[ORM\ManyToOne(targetEntity: Serie::class, fetch: 'EAGER', inversedBy: 'items')]
    private ?Serie $serie = null;

    public function getSerie(): ?Serie
    {
        return $this->serie;
    }

    public function setSerie(?Serie $serie): self
    {
        $this->serie = $serie;

        return $this;
    }
}
