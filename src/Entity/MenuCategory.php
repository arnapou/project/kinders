<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: \App\Repository\MenuCategoryRepository::class)]
#[ORM\Index(columns: ['created_at'], name: 'created_at')]
#[ORM\Index(columns: ['updated_at'], name: 'updated_at')]
#[ORM\Index(columns: ['slug'], name: 'slug')]
#[ORM\Index(columns: ['name'], name: 'name')]
class MenuCategory extends BaseEntity
{
    final public const ITEMS_SORTING = [
        'sorting' => 'ASC',
        'minYear' => 'DESC',
        'name' => 'ASC',
    ];

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: MenuItem::class, orphanRemoval: true)]
    private DoctrineCollection $items;

    #[ORM\Column(type: 'string', length: 40)]
    protected string $sorting = '';

    #[ORM\Column(type: 'boolean')]
    private bool $sidebar = true;

    public function __construct()
    {
        parent::__construct();
        $this->items = new ArrayCollection();
    }

    public function getItems(): ReadableCollection
    {
        if ($this->items instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(self::ITEMS_SORTING);

            return $this->items->matching($criteria);
        }

        return $this->items;
    }

    public function addItem(MenuItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setCategory($this);
        }

        return $this;
    }

    public function removeItem(MenuItem $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getCategory() === $this) {
                $item->setCategory(null);
            }
        }

        return $this;
    }

    public function getSorting(): string
    {
        return $this->sorting;
    }

    public function setSorting(string $sorting): self
    {
        $this->sorting = $sorting;

        return $this;
    }

    public function getSidebar(): ?bool
    {
        return $this->sidebar;
    }

    public function setSidebar(bool $bool): self
    {
        $this->sidebar = $bool;

        return $this;
    }
}
