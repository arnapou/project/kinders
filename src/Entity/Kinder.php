<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Exception\KinderVirtualException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: \App\Repository\KinderRepository::class)]
#[ORM\Index(columns: ['created_at'], name: 'created_at')]
#[ORM\Index(columns: ['updated_at'], name: 'updated_at')]
#[ORM\Index(columns: ['realsorting'], name: 'realsorting')]
#[ORM\Index(columns: ['slug'], name: 'slug')]
#[ORM\Index(columns: ['name'], name: 'name')]
#[ORM\Index(columns: ['quantity_owned'], name: 'quantity_owned')]
#[ORM\Index(columns: ['quantity_double'], name: 'quantity_double')]
#[ORM\Index(columns: ['reference'], name: 'reference')]
#[ORM\Index(columns: ['looking_for'], name: 'looking_for')]
#[ORM\Index(columns: ['year'], name: 'year')]
class Kinder extends BaseItem
{
    final public const BPZS_SORTING = ['name' => 'ASC'];
    final public const ZBAS_SORTING = ['name' => 'ASC'];

    #[ORM\OneToMany(mappedBy: 'kinder', targetEntity: BPZ::class, orphanRemoval: true)]
    private DoctrineCollection $bpzs;

    #[ORM\OneToMany(mappedBy: 'kinder', targetEntity: ZBA::class, orphanRemoval: true)]
    private DoctrineCollection $zbas;

    #[ORM\ManyToOne(targetEntity: Serie::class, fetch: 'EAGER', inversedBy: 'kinders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Serie $serie = null;

    #[ORM\ManyToOne(targetEntity: self::class, fetch: 'EAGER', inversedBy: 'virtuals')]
    private ?Kinder $original = null;

    #[ORM\OneToMany(targetEntity: self::class, mappedBy: 'original')]
    private DoctrineCollection $virtuals;

    public function __construct()
    {
        parent::__construct();
        $this->bpzs = new ArrayCollection();
        $this->zbas = new ArrayCollection();
        $this->virtuals = new ArrayCollection();
    }

    public function getBpzs(): ReadableCollection
    {
        if ($this->bpzs instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(self::BPZS_SORTING);

            return $this->bpzs->matching($criteria);
        }

        return $this->bpzs;
    }

    public function getZbas(): ReadableCollection
    {
        if ($this->zbas instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(self::ZBAS_SORTING);

            return $this->zbas->matching($criteria);
        }

        return $this->zbas;
    }

    public function getSerie(): ?Serie
    {
        return $this->serie;
    }

    public function setSerie(Serie $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getOriginal(): ?self
    {
        return $this->original;
    }

    public function setOriginal(self $original): self
    {
        if ($original->getId() === $this->getId()) {
            throw new KinderVirtualException('Le kinder ne peut pas être virtuel de lui-même.');
        }
        if ($original->isVirtual()) {
            throw new KinderVirtualException("Le kinder ne peut pas être virtuel d'un autre virtuel.");
        }
        if ($this->hasVirtuals()) {
            throw new KinderVirtualException('Le kinder ne peut pas être virtuel car il a déjà des virtuels de lui-même.');
        }
        $this->original = $original;

        return $this;
    }

    public function getVirtuals(): DoctrineCollection
    {
        return $this->virtuals;
    }

    public function isVirtual(): bool
    {
        return (bool) $this->original;
    }

    public function getAttributes(array $types = []): ReadableCollection
    {
        return $this->original ? $this->original->getAttributes($types) : parent::getAttributes($types);
    }

    public function getImage(int $num = 0): ?Image
    {
        return $this->original ? $this->original->getImage($num) : parent::getImage($num);
    }

    public function getImages(): ReadableCollection
    {
        return $this->original ? $this->original->getImages() : parent::getImages();
    }

    public function getComment(): string
    {
        return parent::getComment() ?: ($this->original ? $this->original->getComment() : '');
    }

    public function getDescription(): string
    {
        return parent::getDescription() ?: ($this->original ? $this->original->getDescription() : '');
    }

    public function getVariante(): string
    {
        return parent::getVariante() ?: ($this->original ? $this->original->getVariante() : '');
    }

    private function hasVirtuals(): bool
    {
        return !$this->virtuals->isEmpty();
    }
}
