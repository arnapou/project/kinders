<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: \App\Repository\CountryRepository::class)]
#[ORM\Index(columns: ['created_at'], name: 'created_at')]
#[ORM\Index(columns: ['updated_at'], name: 'updated_at')]
#[ORM\Index(columns: ['slug'], name: 'slug')]
#[ORM\Index(columns: ['name'], name: 'name')]
#[ORM\Index(columns: ['abbr'], name: 'abbr')]
class Country extends BaseEntity
{
    #[ORM\Column(type: 'string', length: 10)]
    private string $abbr = '';

    #[ORM\Column(type: 'string', length: 40)]
    private string $sorting = '';

    public function getAbbr(): string
    {
        return $this->abbr;
    }

    public function setAbbr(string $abbr): void
    {
        $this->abbr = $abbr;
    }

    public function getSorting(): string
    {
        return $this->sorting;
    }

    public function setSorting(string $sorting): self
    {
        $this->sorting = $sorting;

        return $this;
    }
}
