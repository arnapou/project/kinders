<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\ImageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;

use function in_array;

#[ORM\MappedSuperclass]
abstract class BaseItem extends BaseEntity
{
    final public const ATTRIBUTES_SORTING = ['type' => 'ASC', 'name' => 'ASC'];

    #[ORM\Column(type: 'integer')]
    protected int $quantityOwned = 0;

    #[ORM\Column(type: 'integer')]
    protected int $quantityDouble = 0;

    #[ORM\Column(type: 'integer')]
    protected int $quantityEchange = 0;

    #[ORM\Column(type: 'string', length: 40)]
    protected string $reference = '';

    #[ORM\Column(type: 'string', length: 40)]
    protected string $sorting = '';

    #[ORM\Column(type: 'string', length: 100)]
    protected string $realsorting = '#';

    #[ORM\Column(type: 'boolean')]
    protected bool $lookingFor = false;

    #[ORM\Column(type: 'integer')]
    protected int $year = 0;

    #[ORM\ManyToMany(targetEntity: Image::class)]
    protected DoctrineCollection $images;

    #[ORM\ManyToMany(targetEntity: Attribute::class)]
    protected DoctrineCollection $attributes;

    #[ORM\Column(type: 'text')]
    protected string $variante = '';

    public function __construct()
    {
        parent::__construct();
        $this->images = new ArrayCollection();
        $this->attributes = new ArrayCollection();
    }

    public function getQuantityOwned(): int
    {
        return $this->quantityOwned;
    }

    public function setQuantityOwned(int $quantityOwned): self
    {
        $this->quantityOwned = $quantityOwned;

        return $this;
    }

    public function getQuantityDouble(): int
    {
        return $this->quantityDouble;
    }

    public function setQuantityDouble(int $quantityDouble): self
    {
        $this->quantityDouble = $quantityDouble;

        return $this;
    }

    public function getQuantityEchange(): int
    {
        return $this->quantityEchange;
    }

    public function setQuantityEchange(int $quantityEchange): self
    {
        $this->quantityEchange = $quantityEchange;

        return $this;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $reference = str_replace(',', '.', $reference);
        $this->reference = $reference;
        $this->calcRealsorting();

        return $this;
    }

    public function getSorting(): string
    {
        return $this->sorting;
    }

    public function setSorting(string $sorting): self
    {
        $this->sorting = $sorting;
        $this->calcRealsorting();

        return $this;
    }

    public function isLookingFor(): bool
    {
        return $this->lookingFor;
    }

    public function setLookingFor(bool $lookingFor): self
    {
        $this->lookingFor = $lookingFor;

        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function calcRealsorting(): self
    {
        $this->realsorting = (string) preg_replace_callback(
            '!(\d+)!',
            static fn ($matches) => sprintf('%04d', $matches[1]),
            $this->sorting . '#' . $this->reference
        );

        return $this;
    }

    public function getRealsorting(): string
    {
        return $this->realsorting;
    }

    public function getImages(): ReadableCollection
    {
        if ($this->images instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(['name' => 'ASC']);

            return $this->images->matching($criteria);
        }

        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            if (!$image->getType()) {
                $image->setType(ImageRepository::getTypeFrom($this));
            }
            $this->images[] = $image;
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }

        return $this;
    }

    public function getImage(int $num = 0): ?Image
    {
        $i = 0;
        foreach ($this->getImages() as $image) {
            if ($i === $num) {
                return $image instanceof Image ? $image : null;
            }
            ++$i;
        }

        return null;
    }

    public function getAttributes(array $types = []): ReadableCollection
    {
        if ($this->attributes instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(self::ATTRIBUTES_SORTING);

            return $this->attributes->matching($criteria)->filter(
                fn (mixed $attr) => !([] !== $types && $attr instanceof Attribute)
                    || in_array($attr->getType(), $types, true)
            );
        }

        return $this->attributes;
    }

    public function addAttribute(Attribute $attribute): self
    {
        if (!$this->attributes->contains($attribute)) {
            $this->attributes[] = $attribute;
        }

        return $this;
    }

    public function removeAttribute(Attribute $attribute): self
    {
        if ($this->attributes->contains($attribute)) {
            $this->attributes->removeElement($attribute);
        }

        return $this;
    }

    public function hasAttribute(string $type, string $name = null): bool
    {
        $type = strtolower($type);
        $name = strtolower((string) $name);
        foreach ($this->attributes as $attribute) {
            if ($attribute instanceof Attribute
                && strtolower($attribute->getType()) === $type
                && (!$name || strtolower($attribute->getName()) === $name)
            ) {
                return true;
            }
        }

        return false;
    }

    public function getVariante(): string
    {
        return $this->variante;
    }

    public function setVariante(string $variante): self
    {
        $this->variante = $variante;

        return $this;
    }
}
