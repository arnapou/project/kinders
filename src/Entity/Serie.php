<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: \App\Repository\SerieRepository::class)]
#[ORM\Index(columns: ['created_at'], name: 'created_at')]
#[ORM\Index(columns: ['updated_at'], name: 'updated_at')]
#[ORM\Index(columns: ['realsorting'], name: 'realsorting')]
#[ORM\Index(columns: ['slug'], name: 'slug')]
#[ORM\Index(columns: ['name'], name: 'name')]
#[ORM\Index(columns: ['quantity_owned'], name: 'quantity_owned')]
#[ORM\Index(columns: ['quantity_double'], name: 'quantity_double')]
#[ORM\Index(columns: ['reference'], name: 'reference')]
#[ORM\Index(columns: ['looking_for'], name: 'looking_for')]
#[ORM\Index(columns: ['year'], name: 'year')]
class Serie extends BaseItem
{
    final public const KINDER_SORTING = ['realsorting' => 'ASC', 'name' => 'ASC'];
    final public const PIECES_SORTING = ['realsorting' => 'ASC', 'name' => 'ASC'];
    final public const ITEMS_SORTING = ['realsorting' => 'ASC', 'name' => 'ASC'];

    #[ORM\ManyToOne(targetEntity: Catalog::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Catalog $catalog = null;

    #[ORM\Column(type: 'string', length: 100)]
    private string $catalogPage = '';

    #[ORM\ManyToOne(targetEntity: Country::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Country $country = null;

    /** @var DoctrineCollection<int, Kinder> */
    #[ORM\OneToMany(mappedBy: 'serie', targetEntity: Kinder::class, orphanRemoval: true)]
    private DoctrineCollection $kinders;

    /** @var DoctrineCollection<int, Piece> */
    #[ORM\OneToMany(mappedBy: 'serie', targetEntity: Piece::class, orphanRemoval: true)]
    private DoctrineCollection $pieces;

    /** @var DoctrineCollection<int, Item> */
    #[ORM\OneToMany(mappedBy: 'serie', targetEntity: Item::class, orphanRemoval: true)]
    private DoctrineCollection $items;

    #[ORM\ManyToOne(targetEntity: Collection::class, fetch: 'EAGER', inversedBy: 'series')]
    private ?Collection $collection = null;

    public function __construct()
    {
        parent::__construct();
        $this->kinders = new ArrayCollection();
        $this->items = new ArrayCollection();
        $this->pieces = new ArrayCollection();
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCatalog(): ?Catalog
    {
        return $this->catalog;
    }

    public function setCatalog(?Catalog $catalog): self
    {
        $this->catalog = $catalog;

        return $this;
    }

    public function getMaxBpzCount(): int
    {
        $max = 0;
        foreach ($this->kinders as $kinder) {
            $max = max($max, $kinder->getBpzs()->count());
        }

        return $max;
    }

    public function getMaxZbaCount(): int
    {
        $max = 0;
        foreach ($this->kinders as $kinder) {
            $max = max($max, $kinder->getZbas()->count());
        }

        return $max;
    }

    public function getKinders(): ReadableCollection
    {
        if ($this->kinders instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(self::KINDER_SORTING);

            return $this->kinders->matching($criteria);
        }

        return $this->kinders;
    }

    public function getPieces(): ReadableCollection
    {
        if ($this->pieces instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(self::PIECES_SORTING);

            return $this->pieces->matching($criteria);
        }

        return $this->pieces;
    }

    public function addPiece(Piece $piece): self
    {
        if (!$this->pieces->contains($piece)) {
            $this->pieces[] = $piece;
            $piece->setSerie($this);
        }

        return $this;
    }

    public function removePiece(Piece $piece): self
    {
        if ($this->pieces->contains($piece)) {
            $this->pieces->removeElement($piece);
            // set the owning side to null (unless already changed)
            if ($piece->getSerie() === $this) {
                $piece->setSerie(null);
            }
        }

        return $this;
    }

    public function getItems(): ReadableCollection
    {
        if ($this->items instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(self::ITEMS_SORTING);

            return $this->items->matching($criteria);
        }

        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setSerie($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getSerie() === $this) {
                $item->setSerie(null);
            }
        }

        return $this;
    }

    public function getCollection(): ?Collection
    {
        return $this->collection;
    }

    public function setCollection(?Collection $collection): self
    {
        $this->collection = $collection;

        return $this;
    }

    public function isPuzzle(): bool
    {
        return $this->hasAttribute('kinder', 'Puzzle');
    }

    public function getPuzzle(): ?array
    {
        if (!$this->isPuzzle()) {
            return null;
        }
        $kinders = [];
        $w = $h = 0;
        foreach ($this->kinders as $kinder) {
            foreach ($kinder->getAttributes() as $attribute) {
                if ($attribute instanceof Attribute
                    && 'puzzle' === strtolower($attribute->getType())
                    && preg_match('!^(\d+):(\d+)$!', $attribute->getName(), $matches)
                ) {
                    $x = (int) $matches[2];
                    $y = (int) $matches[1];
                    $kinders[$y - 1][$x - 1] = $kinder;

                    $w = max($w, $x);
                    $h = max($h, $y);
                }
            }
        }

        return [
            'width' => $w,
            'height' => $h,
            'kinders' => $kinders,
        ];
    }

    public function getImage(int $num = 0): ?Image
    {
        foreach ($this->getKinders() as $kinder) {
            if ($kinder instanceof BaseItem && $image = $kinder->getImage()) {
                return $image;
            }
        }

        return null;
        // $image = parent::getImage($num);
        // if (0 === $num && null === $image) {
        //     foreach ($this->getKinders() as $kinder) {
        //         if ($image = $kinder->getImage()) {
        //             break;
        //         }
        //     }
        // }
        //
        // return $image;
    }

    public function isComplete(): bool
    {
        foreach ($this->getKinders() as $kinder) {
            if ($kinder instanceof Kinder) {
                if ($kinder->isLookingFor()) {
                    return false;
                }
                foreach ($kinder->getBpzs() as $bpz) {
                    if ($bpz instanceof BaseItem && $bpz->isLookingFor()) {
                        return false;
                    }
                }
                foreach ($kinder->getZbas() as $zba) {
                    if ($zba instanceof BaseItem && $zba->isLookingFor()) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function getCatalogPage(): string
    {
        return $this->catalogPage;
    }

    public function setCatalogPage(string $catalogPage): void
    {
        $this->catalogPage = $catalogPage;
    }
}
