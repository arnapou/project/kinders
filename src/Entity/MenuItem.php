<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: \App\Repository\MenuItemRepository::class)]
class MenuItem extends BaseEntity
{
    final public const ATTRIBUTES_SORTING = ['type' => 'ASC', 'name' => 'ASC'];

    #[ORM\ManyToOne(targetEntity: MenuCategory::class, fetch: 'EAGER', inversedBy: 'items')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MenuCategory $category = null;

    #[ORM\ManyToMany(targetEntity: Attribute::class)]
    protected DoctrineCollection $attributes;

    #[ORM\Column(type: 'string', length: 40)]
    protected string $sorting = '';

    #[ORM\Column(type: 'integer')]
    private int $minYear = 0;

    #[ORM\Column(type: 'integer')]
    private int $maxYear = 0;

    #[ORM\Column(type: 'string', length: 100)]
    private string $routeName = '';

    public function __construct()
    {
        parent::__construct();
        $this->attributes = new ArrayCollection();
    }

    public function getCategory(): ?MenuCategory
    {
        return $this->category;
    }

    public function setCategory(?MenuCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAttributes(): ReadableCollection
    {
        if ($this->attributes instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(self::ATTRIBUTES_SORTING);

            return $this->attributes->matching($criteria);
        }

        return $this->attributes;
    }

    public function addAttribute(Attribute $attribute): self
    {
        if (!$this->attributes->contains($attribute)) {
            $this->attributes[] = $attribute;
        }

        return $this;
    }

    public function removeAttribute(Attribute $attribute): self
    {
        if ($this->attributes->contains($attribute)) {
            $this->attributes->removeElement($attribute);
        }

        return $this;
    }

    public function getMinYear(): ?int
    {
        return $this->routeName ? 0 : $this->minYear;
    }

    public function setMinYear(int $minYear): self
    {
        $this->minYear = $minYear;

        return $this;
    }

    public function getMaxYear(): ?int
    {
        return $this->routeName ? 0 : ($this->maxYear ?: $this->minYear);
    }

    public function setMaxYear(int $maxYear): self
    {
        $this->maxYear = $maxYear;

        return $this;
    }

    public function getSorting(): string
    {
        return $this->sorting;
    }

    public function setSorting(string $sorting): self
    {
        $this->sorting = $sorting;

        return $this;
    }

    public function getRouteName(): ?string
    {
        return $this->routeName;
    }

    public function setRouteName(string $routeName): self
    {
        $this->routeName = $routeName;

        return $this;
    }
}
