<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use ArrayIterator;
use ArrayObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: \App\Repository\CollectionRepository::class)]
#[ORM\Index(columns: ['created_at'], name: 'created_at')]
#[ORM\Index(columns: ['updated_at'], name: 'updated_at')]
#[ORM\Index(columns: ['slug'], name: 'slug')]
#[ORM\Index(columns: ['name'], name: 'name')]
class Collection extends BaseEntity
{
    #[ORM\OneToMany(targetEntity: Serie::class, mappedBy: 'collection')]
    private DoctrineCollection $series;

    public function __construct()
    {
        parent::__construct();
        $this->series = new ArrayCollection();
    }

    public function getSeries(): DoctrineCollection
    {
        if ($this->series instanceof Selectable) {
            $criteria = Criteria::create()->orderBy(['name' => 'ASC', 'year' => 'ASC']);
            $iterator = $this->series->matching($criteria)->getIterator();
            if ($iterator instanceof ArrayObject || $iterator instanceof ArrayIterator) {
                $iterator->uasort(
                    fn (mixed $a, mixed $b) => match (true) {
                        !($a instanceof Serie && $b instanceof Serie) => 0,
                        default => ($a->getCountry()?->getSorting() <=> $b->getCountry()?->getSorting())
                            ?: ($a->getCountry()?->getName() <=> $b->getCountry()?->getName())
                    }
                );

                return new ArrayCollection(iterator_to_array($iterator));
            }
        }

        return $this->series;
    }

    public function addSeries(Serie $series): self
    {
        if (!$this->series->contains($series)) {
            $this->series[] = $series;
            $series->setCollection($this);
        }

        return $this;
    }

    public function removeSeries(Serie $series): self
    {
        if ($this->series->contains($series)) {
            $this->series->removeElement($series);
            // set the owning side to null (unless already changed)
            if ($series->getCollection() === $this) {
                $series->setCollection(null);
            }
        }

        return $this;
    }
}
