<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App;

use function is_array;
use function is_int;
use function is_object;
use function is_scalar;
use function is_string;

use Stringable;
use TypeError;

final class Assert
{
    public static function arrayKey(mixed $value): int|string
    {
        return match (true) {
            is_int($value), is_string($value) => $value,
            default => throw new TypeError('Not an array-key'),
        };
    }

    /**
     * @return class-string
     */
    public static function className(mixed $value): string
    {
        return match (true) {
            is_string($value) && class_exists($value) => $value,
            default => throw new TypeError('Not a class name'),
        };
    }

    public static function object(mixed $value): object
    {
        return match (true) {
            is_object($value) => $value,
            default => throw new TypeError('Not an object'),
        };
    }

    public static function array(mixed $value): array
    {
        return match (true) {
            is_array($value) => $value,
            default => throw new TypeError('Not an array'),
        };
    }

    /**
     * @template Ta
     *
     * @param class-string<Ta> $class
     *
     * @return array<int, Ta>
     */
    public static function arrayOf(mixed $value, string $class): array
    {
        if (!is_array($value)) {
            throw new TypeError('Not an array');
        }
        foreach ($value as $item) {
            if (!is_a($item, $class)) {
                throw new TypeError('Not a ' . $class);
            }
        }

        return $value;
    }

    public static function string(mixed $value): string
    {
        return match (true) {
            $value instanceof Stringable,
            null === $value,
            is_scalar($value),
            is_object($value) && method_exists($value, '__toString') => (string) $value,
            default => throw new TypeError('Not a string'),
        };
    }

    public static function nullableString(mixed $value): ?string
    {
        return match (true) {
            null === $value => null,
            default => self::string($value),
        };
    }

    public static function int(mixed $value): int
    {
        return match (true) {
            null === $value,
            is_scalar($value) => (int) $value,
            default => throw new TypeError('Not an integer'),
        };
    }
}
