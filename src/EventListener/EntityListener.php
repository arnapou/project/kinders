<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\EventListener;

use App\Entity\BaseItem;
use App\Entity\Image;
use App\Repository\ImageRepository;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\UnitOfWork;

class EntityListener
{
    /** @var array<int, Image> */
    private array $images = [];

    public function __construct(
        private readonly ImageRepository $imageRepository
    ) {
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $uow = $args->getObjectManager()->getUnitOfWork();
        $entities = $this->changedEntities($uow);

        foreach ($entities as $entity) {
            if ($entity instanceof Image && ($id = $entity->getId())) {
                $this->images[$id] = $entity;
            }
            if ($entity instanceof BaseItem) {
                foreach ($entity->getImages() as $image) {
                    if ($image instanceof Image && ($id = $image->getId())) {
                        $this->images[$id] = $image;
                    }
                }
            }
        }
    }

    public function postFlush(PostFlushEventArgs $args): void
    {
        $em = $args->getObjectManager();
        $images = $this->images;
        $this->images = [];

        if ($images) {
            foreach ($images as $image) {
                if ($this->checkImage($image)) {
                    $em->persist($image);
                }
            }
            $em->flush();
        }
    }

    private function checkImage(Image $image): bool
    {
        $linked = $this->imageRepository->linked($image);
        if ($linked !== $image->isLinked()) {
            $image->setLinked($linked);

            return true;
        }

        return false;
    }

    private function changedEntities(UnitOfWork $uow): array
    {
        $entities = [];
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            $entities[] = $entity;
        }
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $entities[] = $entity;
        }
        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            $entities[] = $entity;
        }
        foreach ($uow->getScheduledCollectionDeletions() as $collection) {
            if ($collection instanceof PersistentCollection) {
                foreach ($collection->getDeleteDiff() as $entity) {
                    $entities[] = $entity;
                }
                foreach ($collection->getDeleteDiff() as $entity) {
                    $entities[] = $entity;
                }
            }
        }
        foreach ($uow->getScheduledCollectionUpdates() as $collection) {
            if ($collection instanceof PersistentCollection) {
                foreach ($collection->getDeleteDiff() as $entity) {
                    $entities[] = $entity;
                }
                foreach ($collection->getDeleteDiff() as $entity) {
                    $entities[] = $entity;
                }
            }
        }

        return $entities;
    }
}
