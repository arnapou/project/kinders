<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\EventListener;

use App\Assert;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class LastRouteListener implements EventSubscriberInterface
{
    /**
     * For instance:.
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 0]],
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (HttpKernelInterface::MAIN_REQUEST !== $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();
        $session = $request->hasSession() ? $request->getSession() : null;

        $routeName = Assert::string($request->attributes->get('_route'));
        $routeParams = $request->attributes->get('_route_params');

        if ($session && !$this->isBlacklisted($routeName)) {
            $routeData = ['name' => $routeName, 'params' => $routeParams];

            // Do not save same matched route twice
            $thisRoute = $session->get('this_route', []);
            if ($thisRoute == $routeData) {
                return;
            }
            $session->set('last_route', $thisRoute);
            $session->set('this_route', $routeData);
        }
    }

    private function isBlacklisted(?string $routeName): bool
    {
        return null !== $routeName
            && (
                '_' === $routeName[0]
                || false !== stripos($routeName, 'image_thumbnail')
                || false !== stripos($routeName, 'autocomplete')
                || false === stripos($routeName, 'admin_')
            );
    }
}
