<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Presenter;

use App\Entity\Kinder;
use App\Entity\Serie;

/**
 * @method ?Serie getSerie()
 * @method int    getId()
 * @method Kinder realObject()
 */
class KinderPresenter extends ObjectPresenter
{
    /** @var array<string, bool> */
    public array $flag = ['kinder' => false, 'bpz' => false, 'zba' => false];
}
