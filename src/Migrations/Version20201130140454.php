<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201130140454 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_AD8A54A9F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attribute (id BIGINT AUTO_INCREMENT NOT NULL, type VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX slug (slug), INDEX name (name), INDEX type (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bpz (id BIGINT AUTO_INCREMENT NOT NULL, kinder_id BIGINT NOT NULL, quantity_owned INT NOT NULL, quantity_double INT NOT NULL, quantity_echange INT NOT NULL, reference VARCHAR(40) NOT NULL, sorting VARCHAR(40) NOT NULL, realsorting VARCHAR(100) NOT NULL, looking_for TINYINT(1) NOT NULL, year INT NOT NULL, variante LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_2BFD2788B69346BB (kinder_id), INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX realsorting (realsorting), INDEX slug (slug), INDEX name (name), INDEX quantity_owned (quantity_owned), INDEX quantity_double (quantity_double), INDEX reference (reference), INDEX looking_for (looking_for), INDEX year (year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bpz_image (bpz_id BIGINT NOT NULL, image_id BIGINT NOT NULL, INDEX IDX_8E13EA1990AEB3E7 (bpz_id), INDEX IDX_8E13EA193DA5256D (image_id), PRIMARY KEY(bpz_id, image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bpz_attribute (bpz_id BIGINT NOT NULL, attribute_id BIGINT NOT NULL, INDEX IDX_D70848490AEB3E7 (bpz_id), INDEX IDX_D708484B6E62EFA (attribute_id), PRIMARY KEY(bpz_id, attribute_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE catalog (id BIGINT AUTO_INCREMENT NOT NULL, sorting VARCHAR(40) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX slug (slug), INDEX name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE collection (id BIGINT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX slug (slug), INDEX name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id BIGINT AUTO_INCREMENT NOT NULL, abbr VARCHAR(10) NOT NULL, sorting VARCHAR(40) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX slug (slug), INDEX name (name), INDEX abbr (abbr), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image (id BIGINT AUTO_INCREMENT NOT NULL, type VARCHAR(100) NOT NULL, file VARCHAR(100) DEFAULT NULL, size INT DEFAULT NULL, linked TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX slug (slug), INDEX name (name), INDEX type (type), INDEX file (file), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item (id BIGINT AUTO_INCREMENT NOT NULL, serie_id BIGINT DEFAULT NULL, quantity_owned INT NOT NULL, quantity_double INT NOT NULL, quantity_echange INT NOT NULL, reference VARCHAR(40) NOT NULL, sorting VARCHAR(40) NOT NULL, realsorting VARCHAR(100) NOT NULL, looking_for TINYINT(1) NOT NULL, year INT NOT NULL, variante LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_1F1B251ED94388BD (serie_id), INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX realsorting (realsorting), INDEX slug (slug), INDEX name (name), INDEX quantity_owned (quantity_owned), INDEX quantity_double (quantity_double), INDEX reference (reference), INDEX looking_for (looking_for), INDEX year (year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_image (item_id BIGINT NOT NULL, image_id BIGINT NOT NULL, INDEX IDX_EF9A1F8F126F525E (item_id), INDEX IDX_EF9A1F8F3DA5256D (image_id), PRIMARY KEY(item_id, image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_attribute (item_id BIGINT NOT NULL, attribute_id BIGINT NOT NULL, INDEX IDX_F6A0F90B126F525E (item_id), INDEX IDX_F6A0F90BB6E62EFA (attribute_id), PRIMARY KEY(item_id, attribute_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kinder (id BIGINT AUTO_INCREMENT NOT NULL, serie_id BIGINT NOT NULL, original_id BIGINT DEFAULT NULL, quantity_owned INT NOT NULL, quantity_double INT NOT NULL, quantity_echange INT NOT NULL, reference VARCHAR(40) NOT NULL, sorting VARCHAR(40) NOT NULL, realsorting VARCHAR(100) NOT NULL, looking_for TINYINT(1) NOT NULL, year INT NOT NULL, variante LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_75752738D94388BD (serie_id), INDEX IDX_75752738108B7592 (original_id), INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX realsorting (realsorting), INDEX slug (slug), INDEX name (name), INDEX quantity_owned (quantity_owned), INDEX quantity_double (quantity_double), INDEX reference (reference), INDEX looking_for (looking_for), INDEX year (year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kinder_image (kinder_id BIGINT NOT NULL, image_id BIGINT NOT NULL, INDEX IDX_DA166402B69346BB (kinder_id), INDEX IDX_DA1664023DA5256D (image_id), PRIMARY KEY(kinder_id, image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kinder_attribute (kinder_id BIGINT NOT NULL, attribute_id BIGINT NOT NULL, INDEX IDX_70B20338B69346BB (kinder_id), INDEX IDX_70B20338B6E62EFA (attribute_id), PRIMARY KEY(kinder_id, attribute_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu_category (id BIGINT AUTO_INCREMENT NOT NULL, sorting VARCHAR(40) NOT NULL, sidebar TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX slug (slug), INDEX name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu_item (id BIGINT AUTO_INCREMENT NOT NULL, category_id BIGINT NOT NULL, sorting VARCHAR(40) NOT NULL, min_year INT NOT NULL, max_year INT NOT NULL, route_name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_D754D55012469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu_item_attribute (menu_item_id BIGINT NOT NULL, attribute_id BIGINT NOT NULL, INDEX IDX_EF6A0C2F9AB44FE0 (menu_item_id), INDEX IDX_EF6A0C2FB6E62EFA (attribute_id), PRIMARY KEY(menu_item_id, attribute_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE piece (id BIGINT AUTO_INCREMENT NOT NULL, serie_id BIGINT DEFAULT NULL, quantity_owned INT NOT NULL, quantity_double INT NOT NULL, quantity_echange INT NOT NULL, reference VARCHAR(40) NOT NULL, sorting VARCHAR(40) NOT NULL, realsorting VARCHAR(100) NOT NULL, looking_for TINYINT(1) NOT NULL, year INT NOT NULL, variante LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_44CA0B23D94388BD (serie_id), INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX realsorting (realsorting), INDEX slug (slug), INDEX name (name), INDEX quantity_owned (quantity_owned), INDEX quantity_double (quantity_double), INDEX reference (reference), INDEX looking_for (looking_for), INDEX year (year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE piece_image (piece_id BIGINT NOT NULL, image_id BIGINT NOT NULL, INDEX IDX_D78E8534C40FCFA8 (piece_id), INDEX IDX_D78E85343DA5256D (image_id), PRIMARY KEY(piece_id, image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE piece_attribute (piece_id BIGINT NOT NULL, attribute_id BIGINT NOT NULL, INDEX IDX_CAFB627DC40FCFA8 (piece_id), INDEX IDX_CAFB627DB6E62EFA (attribute_id), PRIMARY KEY(piece_id, attribute_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE serie (id BIGINT AUTO_INCREMENT NOT NULL, catalog_id BIGINT DEFAULT NULL, country_id BIGINT NOT NULL, collection_id BIGINT DEFAULT NULL, catalog_page VARCHAR(100) NOT NULL, quantity_owned INT NOT NULL, quantity_double INT NOT NULL, quantity_echange INT NOT NULL, reference VARCHAR(40) NOT NULL, sorting VARCHAR(40) NOT NULL, realsorting VARCHAR(100) NOT NULL, looking_for TINYINT(1) NOT NULL, year INT NOT NULL, variante LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_AA3A9334CC3C66FC (catalog_id), INDEX IDX_AA3A9334F92F3E70 (country_id), INDEX IDX_AA3A9334514956FD (collection_id), INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX realsorting (realsorting), INDEX slug (slug), INDEX name (name), INDEX quantity_owned (quantity_owned), INDEX quantity_double (quantity_double), INDEX reference (reference), INDEX looking_for (looking_for), INDEX year (year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE serie_image (serie_id BIGINT NOT NULL, image_id BIGINT NOT NULL, INDEX IDX_D3130CBD94388BD (serie_id), INDEX IDX_D3130CB3DA5256D (image_id), PRIMARY KEY(serie_id, image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE serie_attribute (serie_id BIGINT NOT NULL, attribute_id BIGINT NOT NULL, INDEX IDX_D230D1CAD94388BD (serie_id), INDEX IDX_D230D1CAB6E62EFA (attribute_id), PRIMARY KEY(serie_id, attribute_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_config (id BIGINT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX slug (slug), INDEX name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zba (id BIGINT AUTO_INCREMENT NOT NULL, kinder_id BIGINT NOT NULL, quantity_owned INT NOT NULL, quantity_double INT NOT NULL, quantity_echange INT NOT NULL, reference VARCHAR(40) NOT NULL, sorting VARCHAR(40) NOT NULL, realsorting VARCHAR(100) NOT NULL, looking_for TINYINT(1) NOT NULL, year INT NOT NULL, variante LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(150) NOT NULL, comment LONGTEXT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_CB596C7FB69346BB (kinder_id), INDEX created_at (created_at), INDEX updated_at (updated_at), INDEX realsorting (realsorting), INDEX slug (slug), INDEX name (name), INDEX quantity_owned (quantity_owned), INDEX quantity_double (quantity_double), INDEX reference (reference), INDEX looking_for (looking_for), INDEX year (year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zba_image (zba_id BIGINT NOT NULL, image_id BIGINT NOT NULL, INDEX IDX_C24CF412E213276D (zba_id), INDEX IDX_C24CF4123DA5256D (image_id), PRIMARY KEY(zba_id, image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zba_attribute (zba_id BIGINT NOT NULL, attribute_id BIGINT NOT NULL, INDEX IDX_8E80C1DFE213276D (zba_id), INDEX IDX_8E80C1DFB6E62EFA (attribute_id), PRIMARY KEY(zba_id, attribute_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bpz ADD CONSTRAINT FK_2BFD2788B69346BB FOREIGN KEY (kinder_id) REFERENCES kinder (id)');
        $this->addSql('ALTER TABLE bpz_image ADD CONSTRAINT FK_8E13EA1990AEB3E7 FOREIGN KEY (bpz_id) REFERENCES bpz (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bpz_image ADD CONSTRAINT FK_8E13EA193DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bpz_attribute ADD CONSTRAINT FK_D70848490AEB3E7 FOREIGN KEY (bpz_id) REFERENCES bpz (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bpz_attribute ADD CONSTRAINT FK_D708484B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251ED94388BD FOREIGN KEY (serie_id) REFERENCES serie (id)');
        $this->addSql('ALTER TABLE item_image ADD CONSTRAINT FK_EF9A1F8F126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE item_image ADD CONSTRAINT FK_EF9A1F8F3DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE item_attribute ADD CONSTRAINT FK_F6A0F90B126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE item_attribute ADD CONSTRAINT FK_F6A0F90BB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kinder ADD CONSTRAINT FK_75752738D94388BD FOREIGN KEY (serie_id) REFERENCES serie (id)');
        $this->addSql('ALTER TABLE kinder ADD CONSTRAINT FK_75752738108B7592 FOREIGN KEY (original_id) REFERENCES kinder (id)');
        $this->addSql('ALTER TABLE kinder_image ADD CONSTRAINT FK_DA166402B69346BB FOREIGN KEY (kinder_id) REFERENCES kinder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kinder_image ADD CONSTRAINT FK_DA1664023DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kinder_attribute ADD CONSTRAINT FK_70B20338B69346BB FOREIGN KEY (kinder_id) REFERENCES kinder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kinder_attribute ADD CONSTRAINT FK_70B20338B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_item ADD CONSTRAINT FK_D754D55012469DE2 FOREIGN KEY (category_id) REFERENCES menu_category (id)');
        $this->addSql('ALTER TABLE menu_item_attribute ADD CONSTRAINT FK_EF6A0C2F9AB44FE0 FOREIGN KEY (menu_item_id) REFERENCES menu_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_item_attribute ADD CONSTRAINT FK_EF6A0C2FB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE piece ADD CONSTRAINT FK_44CA0B23D94388BD FOREIGN KEY (serie_id) REFERENCES serie (id)');
        $this->addSql('ALTER TABLE piece_image ADD CONSTRAINT FK_D78E8534C40FCFA8 FOREIGN KEY (piece_id) REFERENCES piece (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE piece_image ADD CONSTRAINT FK_D78E85343DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE piece_attribute ADD CONSTRAINT FK_CAFB627DC40FCFA8 FOREIGN KEY (piece_id) REFERENCES piece (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE piece_attribute ADD CONSTRAINT FK_CAFB627DB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie ADD CONSTRAINT FK_AA3A9334CC3C66FC FOREIGN KEY (catalog_id) REFERENCES catalog (id)');
        $this->addSql('ALTER TABLE serie ADD CONSTRAINT FK_AA3A9334F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE serie ADD CONSTRAINT FK_AA3A9334514956FD FOREIGN KEY (collection_id) REFERENCES collection (id)');
        $this->addSql('ALTER TABLE serie_image ADD CONSTRAINT FK_D3130CBD94388BD FOREIGN KEY (serie_id) REFERENCES serie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie_image ADD CONSTRAINT FK_D3130CB3DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie_attribute ADD CONSTRAINT FK_D230D1CAD94388BD FOREIGN KEY (serie_id) REFERENCES serie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie_attribute ADD CONSTRAINT FK_D230D1CAB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zba ADD CONSTRAINT FK_CB596C7FB69346BB FOREIGN KEY (kinder_id) REFERENCES kinder (id)');
        $this->addSql('ALTER TABLE zba_image ADD CONSTRAINT FK_C24CF412E213276D FOREIGN KEY (zba_id) REFERENCES zba (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zba_image ADD CONSTRAINT FK_C24CF4123DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zba_attribute ADD CONSTRAINT FK_8E80C1DFE213276D FOREIGN KEY (zba_id) REFERENCES zba (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zba_attribute ADD CONSTRAINT FK_8E80C1DFB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bpz DROP FOREIGN KEY FK_2BFD2788B69346BB');
        $this->addSql('ALTER TABLE bpz_image DROP FOREIGN KEY FK_8E13EA1990AEB3E7');
        $this->addSql('ALTER TABLE bpz_image DROP FOREIGN KEY FK_8E13EA193DA5256D');
        $this->addSql('ALTER TABLE bpz_attribute DROP FOREIGN KEY FK_D70848490AEB3E7');
        $this->addSql('ALTER TABLE bpz_attribute DROP FOREIGN KEY FK_D708484B6E62EFA');
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251ED94388BD');
        $this->addSql('ALTER TABLE item_image DROP FOREIGN KEY FK_EF9A1F8F126F525E');
        $this->addSql('ALTER TABLE item_image DROP FOREIGN KEY FK_EF9A1F8F3DA5256D');
        $this->addSql('ALTER TABLE item_attribute DROP FOREIGN KEY FK_F6A0F90B126F525E');
        $this->addSql('ALTER TABLE item_attribute DROP FOREIGN KEY FK_F6A0F90BB6E62EFA');
        $this->addSql('ALTER TABLE kinder DROP FOREIGN KEY FK_75752738D94388BD');
        $this->addSql('ALTER TABLE kinder DROP FOREIGN KEY FK_75752738108B7592');
        $this->addSql('ALTER TABLE kinder_image DROP FOREIGN KEY FK_DA166402B69346BB');
        $this->addSql('ALTER TABLE kinder_image DROP FOREIGN KEY FK_DA1664023DA5256D');
        $this->addSql('ALTER TABLE kinder_attribute DROP FOREIGN KEY FK_70B20338B69346BB');
        $this->addSql('ALTER TABLE kinder_attribute DROP FOREIGN KEY FK_70B20338B6E62EFA');
        $this->addSql('ALTER TABLE menu_item DROP FOREIGN KEY FK_D754D55012469DE2');
        $this->addSql('ALTER TABLE menu_item_attribute DROP FOREIGN KEY FK_EF6A0C2F9AB44FE0');
        $this->addSql('ALTER TABLE menu_item_attribute DROP FOREIGN KEY FK_EF6A0C2FB6E62EFA');
        $this->addSql('ALTER TABLE piece DROP FOREIGN KEY FK_44CA0B23D94388BD');
        $this->addSql('ALTER TABLE piece_image DROP FOREIGN KEY FK_D78E8534C40FCFA8');
        $this->addSql('ALTER TABLE piece_image DROP FOREIGN KEY FK_D78E85343DA5256D');
        $this->addSql('ALTER TABLE piece_attribute DROP FOREIGN KEY FK_CAFB627DC40FCFA8');
        $this->addSql('ALTER TABLE piece_attribute DROP FOREIGN KEY FK_CAFB627DB6E62EFA');
        $this->addSql('ALTER TABLE serie DROP FOREIGN KEY FK_AA3A9334CC3C66FC');
        $this->addSql('ALTER TABLE serie DROP FOREIGN KEY FK_AA3A9334F92F3E70');
        $this->addSql('ALTER TABLE serie DROP FOREIGN KEY FK_AA3A9334514956FD');
        $this->addSql('ALTER TABLE serie_image DROP FOREIGN KEY FK_D3130CBD94388BD');
        $this->addSql('ALTER TABLE serie_image DROP FOREIGN KEY FK_D3130CB3DA5256D');
        $this->addSql('ALTER TABLE serie_attribute DROP FOREIGN KEY FK_D230D1CAD94388BD');
        $this->addSql('ALTER TABLE serie_attribute DROP FOREIGN KEY FK_D230D1CAB6E62EFA');
        $this->addSql('ALTER TABLE zba DROP FOREIGN KEY FK_CB596C7FB69346BB');
        $this->addSql('ALTER TABLE zba_image DROP FOREIGN KEY FK_C24CF412E213276D');
        $this->addSql('ALTER TABLE zba_image DROP FOREIGN KEY FK_C24CF4123DA5256D');
        $this->addSql('ALTER TABLE zba_attribute DROP FOREIGN KEY FK_8E80C1DFE213276D');
        $this->addSql('ALTER TABLE zba_attribute DROP FOREIGN KEY FK_8E80C1DFB6E62EFA');
        $this->addSql('DROP TABLE admin_user');
        $this->addSql('DROP TABLE attribute');
        $this->addSql('DROP TABLE bpz');
        $this->addSql('DROP TABLE bpz_image');
        $this->addSql('DROP TABLE bpz_attribute');
        $this->addSql('DROP TABLE catalog');
        $this->addSql('DROP TABLE collection');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE item');
        $this->addSql('DROP TABLE item_image');
        $this->addSql('DROP TABLE item_attribute');
        $this->addSql('DROP TABLE kinder');
        $this->addSql('DROP TABLE kinder_image');
        $this->addSql('DROP TABLE kinder_attribute');
        $this->addSql('DROP TABLE menu_category');
        $this->addSql('DROP TABLE menu_item');
        $this->addSql('DROP TABLE menu_item_attribute');
        $this->addSql('DROP TABLE piece');
        $this->addSql('DROP TABLE piece_image');
        $this->addSql('DROP TABLE piece_attribute');
        $this->addSql('DROP TABLE serie');
        $this->addSql('DROP TABLE serie_image');
        $this->addSql('DROP TABLE serie_attribute');
        $this->addSql('DROP TABLE site_config');
        $this->addSql('DROP TABLE zba');
        $this->addSql('DROP TABLE zba_image');
        $this->addSql('DROP TABLE zba_attribute');
    }
}
