Kinders
====================

![pipeline](https://gitlab.com/arnapou/project/kinders/badges/main/pipeline.svg)
![coverage](https://gitlab.com/arnapou/project/kinders/badges/main/coverage.svg)

Installation
--------------------

```bash
composer create-project arnapou/kinders
```

packagist 👉️ [arnapou/kinders](https://packagist.org/packages/arnapou/kinders)

Informations
--------------------

Ce repo est le code source de http://kinders.arnapou.net/

Le code est ouvert, vous pouvez l'utiliser comme vous voulez, mais je ne supporterai 
pas les éventuels problèmes que vous auriez.

En bref, il s'agit d'un site de gestion de collection de kinders surprise, collection
que fait mon épouse.


Schema basique des liaisons des entités
--------------------

```
                                         ╭──╮
                                         │  ▼      ╭──── BPZ ◀────╮
Collection ◀─────╮                ╭──── Kinder ◀───┤              │
                 ├───── Serie ◀───┤         ▲      ╰──── ZBA ◀────┤
Country ◀────────╯                │         │                     │   
                                  │         ╰─────────────────────┤   ╭───▶ Attribute
MenuCategory ◀────── MenuItem     │                               ├───┤
                                  ├───────────────────────────────┤   ╰───▶ Image 
AdminUser                         │                               │     
                                  ├──── Item ◀────────────────────┤
SiteConfig                        │                               │
                                  ╰──── Piece ◀───────────────────╯
```

Hiérarchie des entités
--------------------

```
AdminUser
BaseEntity
 ├─ Attribute
 ├─ BaseItem
 │  ├─ BPZ
 │  ├─ Item
 │  ├─ Kinder
 │  ├─ Piece
 │  ├─ Serie
 │  └─ ZBA
 │
 ├─ Collection
 ├─ Country
 ├─ Image
 ├─ MenuCategory
 ├─ MenuItem
 └─ SiteConfig
```

Champs communs de `BaseEntity` :

    int       id
    datetime  createdAt
    datetime  updatedAt
    string    name
    string    slug
    string    comment
    string    description

Champs communs de `BaseItem` :

    int       quantityOwned
    int       quantityDouble
    int       year
    bool      lookingFor
    string    reference
    string    sorting
    string    realsorting
    string    variante
    ◀─▶       images          App\Entity\Image   
    ◀─▶       attributes      App\Entity\Attribute


Changelog versions
--------------------

| Start      | Tag, Branch | Php | Symfony | 
|------------|-------------|-----|---------|
| 23/12/2022 | 5.x, main   | 8.2 | 5.4     |
| 01/02/2022 | 4.x         | 8.1 | 5.4     |
| 10/05/2021 | 3.x         | 8.0 | 4.4     |
| 09/05/2020 | 2.x         | 7.4 | 4.4     |
| 14/04/2019 | 1.x         | 7.2 | 4.2     |
