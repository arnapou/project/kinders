.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

COMPOSER_OPTS=--no-interaction --no-progress --optimize-autoloader --classmap-authoritative --no-scripts

all: install cs sa test coverage ## code style + analysis + test

cache-clear:
	find var/cache -type f -delete
	composer run-script auto-scripts

sa: ## static analysis
	vendor/bin/phpstan

cs: ## code style
	PHP_CS_FIXER_IGNORE_ENV=1 vendor/bin/php-cs-fixer fix --using-cache=no

test: ## phpunit
	vendor/bin/phpunit --testdox --colors=always

coverage: ## phpunit coverage
ifdef CI_JOB_NAME
	vendor/bin/phpunit --colors=never --coverage-text
else
	vendor/bin/phpunit --colors=always --coverage-text --coverage-html tests/coverage
endif

install: ## composer install
	@touch .env.local.php
	composer install $(COMPOSER_OPTS) --quiet

update: ## composer update
	@touch .env.local.php
	composer update $(COMPOSER_OPTS)

build: ## build docker image
	docker build -t registry.gitlab.com/arnapou/kinders:latest .
