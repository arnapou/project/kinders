<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use App\Entity\Image;

return static function (ContainerConfigurator $container) {
    $container->parameters()
        ->set('vich.field', Image::VICH_FIELD)
        ->set('vich.uri_prefix', Image::PUBLIC_DIR)
        ->set('vich.upload_destination', '%kernel.project_dir%/public' . Image::PUBLIC_DIR);
};
