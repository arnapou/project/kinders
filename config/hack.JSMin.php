<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace JSMin;

/**
 * Hack pour fixer une deprecation PHP 8.1.
 *
 * ord(): Passing null to parameter #1 ($character) of type string is deprecated
 */
function ord(?string $character): int
{
    return null === $character ? 0 : \ord($character);
}
