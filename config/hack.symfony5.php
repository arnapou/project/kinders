<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Kinders package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Hack\Symfony5;

use Symfony\Component\HttpFoundation\Request;

/**
 * Mark Request::get() internal, use explicit input sources instead.
 */
function sf5Get(?Request $request, string $key, $default = null): mixed
{
    if (null === $request) {
        return null;
    }

    if ($request !== $result = $request->attributes->get($key, $request)) {
        return $result;
    }

    if ($request->query->has($key)) {
        return $request->query->all()[$key];
    }

    if ($request->request->has($key)) {
        return $request->request->all()[$key];
    }

    return $default;
}
